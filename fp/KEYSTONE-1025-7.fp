
Element["" "" "B?" "" 30mm 30.5mm -2.4mm -0.7mm 0 100 ""]
(
	Pin[-325mil -188mil 3mm 0.5mm 3.15mm 1.5mm "" "3" "edge2"]
	Pin[325mil -187mil 3mm 0.5mm 3.15mm 1.5mm "" "3" "edge2"]
	Pin[0.0000 375mil 3mm 0.5mm 3.15mm 1.5mm "" "3" "edge2"]
	Pin[-15.1mm 0 3mm 0.5mm 3.15mm 1.5mm "" "1" "square,edge2"]
	Pin[5.4mm 0 3mm 0.5mm 3.15mm 1.5mm "" "2" "square,edge2"]
	ElementLine [-17mm -3mm -17mm 3mm 10mil]
	ElementLine [-17mm 3mm -14mm 3mm 10mil]
	ElementLine [-17mm -3mm -14mm -3mm 10mil]
	ElementArc [0 0 14mm 14mm 0 360 10mil]
	)
