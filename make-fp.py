#! /usr/bin/python3

from fp import *

make(SIL,  (3,4))
make(SOIC, (14,))
make(HEADER, (2, 10,))
make(SOT, (3,5))
make(SOD, ("C0603", "P1206"))
part(SUBD, n=9, sex="PINS", mounting=None)
