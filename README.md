# Radio Weather Station

- MS5534C Pressure Sensor
- ATtiny404
- WRL-10534 LPD433 Radio transmitter

![Schematics](turbo_sch.png)

![Layout](gerber/turbo.png)