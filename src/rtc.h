//
// rtc.h
//

#include <stdint.h>

volatile extern uint32_t clock;
volatile extern uint8_t clock_tick;
void init_rtc(uint8_t p);
