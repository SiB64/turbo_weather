//
// cmd.c
//

#include "bate.h"
#include "rtc.h"
#include <string.h>

#ifdef NOCMD
// no space for this
void parse_command(uint8_t *s, uint8_t n) {}
#else

uint8_t rx_params[8];

#if 0

__attribute__ ((noinline, noclone))
uint8_t parse_hex_nibble(uint8_t c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 0x0a;
	return 0xf0;
}

static inline
uint8_t parse_rx_params(uint8_t *s, uint8_t n)
{
	uint8_t p;
	p = 0;
	while (n) {
		while (n) {
			if (*s != ' ')
				break;
			n--;
			s++;
		}
		if (!n || p >= sizeof(rx_params))
			break;
		uint8_t d = parse_hex_nibble(*s);
		if (d==0xf0)
			break;
		n--;
		s++;
		if (n) {
			uint8_t h = 0;
			h = parse_hex_nibble(*s);
			if (h != 0xf0) {
				n--;
				s++;
				d <<= 4;
				d |= h;
			}
		}
		rx_params[p++] = d;
		DEBUG_POKE(rxhex, d);
	}
	if (!n || *s != '\n')
		p = 0;
	DEBUG_POKE(rxpar, p);
	return p;
}

#else

static inline
uint8_t parse_rx_params(uint8_t *s, uint8_t n)
{
	uint8_t p;
	__asm__("\n"
		"1:					\n"
		"	clr	%[p]			\n"
		"	tst	%[n]			\n"
		"	breq 8f				\n"
		"	ldi	r26, lo8(rx_params)	\n"
		"	ldi	r27, hi8(rx_params)	\n"
		"2:					\n"
		"	ld	r21, Z+			\n"
		"	cpi	r21, ' '		\n"
		"	breq 6f				\n"
		"	cpi	r21, '\\n'		\n"
		"	breq 8f				\n"
		"	rcall	nibble			\n"
		"	brcc 7f				\n"
		"	mov	r25, r21		\n"
		"	subi	%[n], 1			\n"
		"	breq 7f				\n"
		"	ld	r21, Z			\n"
		"	rcall	nibble			\n"
		"	brcc 5f				\n"
		"	subi	%[n], 1			\n"
		"	breq	7f			\n"
		"	adiw	r30, 1			\n"
		"	swap	r25			\n"
		"	or	r25, r21		\n"
		"5:					\n"
		"	cpi	%[p], 8	; sz rx_params	\n"
		"	brcc 7f				\n"
		"	subi	%[p], -1		\n"
		"	st	X+, r25			\n"
#ifdef DEBUG
		"	sts	debug_data + 6, r25	\n"
		"	sts	debug_data + 9, %[p]	\n"
#endif
		"	rjmp 2b				\n"
		"					\n"
		"nibble:				\n"		
		"	subi	r21, '0'		\n"
		"	cpi	r21, 10			\n"
		"	brcs 9f				\n"
		"	subi	r21, 'a'-'0'		\n"
		"	cpi	r21, 6			\n"
		"	brcc 9f				\n"
		"	subi	r21, -10		\n"
		"	sec				\n"
		"9:	ret				\n"
		"					\n"
		"6:					\n"
		"	subi	%[n], 1			\n"
		"	brne 2b				\n"
		"7:					\n"
		"	clr	%[p]			\n"
		"8:					\n"
		: [p] "=a" (p), // r24      (r16…)
		  [s] "+z" (s), // r30,r31
		  [n] "+a" (n)  // r22      (r16…)
		: : "r21", "r25", "r26", "r27"
		);
	DEBUG_POKE(rxpar, p);
	return p;
}
#endif

void parse_command(uint8_t *s, uint8_t n)
{
	if (!n || n >= 15)
		goto fail;

	uint8_t cmd = *s++;
	uint8_t p = parse_rx_params(s, n-1);
	uint8_t *pp = rx_params;

	uint8_t *a = (uint8_t *)(((uint16_t)pp[0] << 8) | pp[1]);

	if (cmd == 'R' && p==1) 
		__asm__("\n"
			"	ldi	r18, 1		\n"
			"	out	%[ccp], %[p]	\n"
			"	sts	%[swrr], r18	\n"
			:
			: [p]    "r" (pp[0]),
			  [ccp]  "n" (_SFR_IO_ADDR(CCP)),
			  [swrr] "n" (&RSTCTRL.SWRR)
			: "r18"
			);

	if (p >= 3 && p < 8 && (
		    cmd == 'C' && pp[0]==0xba
		    || cmd == 'E'
		    || cmd == 'U')) {
		uint8_t *e = (uint8_t*) &config;
		if (cmd=='E')
			e = (uint8_t*) EEPROM_START;
		if (cmd=='U')
			e = (uint8_t*) &USERROW;
		e += pp[1];
		pp[p] = *e;
		memcpy(e, pp+2, p-2);
		if (cmd != 'C')
			__asm__("\n"
				"	ldi	r24, %[erwp]	\n"
				"	out	%[ccp], %[p]	\n"
				"	sts	%[ctrl], r24	\n"
				:
				: [p]    "r" (pp[0]),
				  [ccp]  "n" (_SFR_IO_ADDR(CCP)),
				  [ctrl] "n" (&NVMCTRL.CTRLA),
				  [erwp] "n" (NVMCTRL_CMD_PAGEERASEWRITE_gc)
				: "r24"
				);
	}
	else if (cmd == 'T' && p==1) {
		immediate += pp[0];
		pp[1] = immediate;
	}
	else if (cmd == 'M' && p==2) {
		pp[2] = *a;
	}
	else if (cmd == 'W' && p==3) {
		pp[3] = *a;
		*a = pp[2];
	}
	else if (cmd == 'K' && p==4) {
		cli();
		pp[4] = clock_tick;
		clock = *(uint32_t*)pp;
		sei();
	}
	else if (cmd == 'D' && p==1) {
		pp[1] = test_calib;
		test_calib = pp[0];
	}
	else
		goto fail;

	send_char('R');
	send_char('!');
	send_hex(cmd, pp, p+1, 1);
	return;
fail:
	send_str("R?\n");
}

#endif
