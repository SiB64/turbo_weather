//
// bate.h
//

#include "uart.h"

////////////////////////////////////////////////////////////////////////////////
//
//  Configuration

struct config {
	uint8_t   magic;
	uint8_t   version;
	uint8_t   triggers;
	uint8_t   send;
	uint8_t   power;
	uint8_t   calib_test;
	uint8_t   spi_div;
	uint8_t   mclk_delay;
	uint8_t   period;
	uint8_t   confp;
	uint8_t   cpu_clk;
	uint8_t   mclk_period;
	uint16_t  baud_div;
	uint8_t   uart_mode;
	uint8_t   pit_period;
	uint8_t	  immediate;
	uint8_t   pad[7];
	char      line_preable[7];
	uint8_t   zero;
};

enum magic_flags {
	USE_USERROW       = 0xBA,
	USE_VERSION       = 0x09,
};
enum power_flags {
	POWER_DOWN        = 0x01,
	POWER_DOWN_CLI    = 0x02,
	STOP_MCLK         = 0x04,
	POWER_LED         = 0x08,
	POWER_STDBY       = 0x10,
	POWER_RX          = 0x20,
	POWER_RF          = 0x40,
	POWER_LINE        = 0x80,
};
enum send_flags {
	SEND_CONFIG       = 0x01,
	SEND_BATED        = 0x02,
	SEND_BATEW        = 0x04,
	SEND_CLOCK        = 0x08,
	SEND_CALIB        = 0x10,
	SEND_ADC_HEX      = 0x20,
	SEND_ADC_VOLT     = 0x40,
	SEND_ADC          = 0x60,
	SEND_DEBUG        = 0x80,
};
enum trigger_flags {
	TRIGGER_ONCE  = 0x01,
	TRIGGER_CONT  = 0x02,
	TRIGGER_UART  = 0x04,
	TRIGGER_CLOCK = 0x08,
	TRIGGER_BREAK = 0x10,
	TRIGGER_IMMED = 0x20,
};
enum UART_flags { // USART0.CTRLB
	UART_Tx       = /* 0x40, */ USART_TXEN_bm,
	UART_Rx       = /* 0x80, */ USART_RXEN_bm,
	UART_SFD      = /* 0x10, */ USART_SFDEN_bm,
	UART_ODME     = /* 0x08, */ USART_ODME_bm,
	UART_CLKx2    = /* 0x02, */ 1 << USART_RXMODE_0_bp,
	UART_GAUTO    = /* 0x04, */ 2 << USART_RXMODE_0_bp,
	UART_LAUTO    = /* 0x06, */ 3 << USART_RXMODE_0_bp,
};

extern struct config config;
extern uint8_t immediate;
extern uint8_t test_calib;

#ifndef NODEBUG
#   define  DEBUG
#endif

#ifdef DEBUG

#include <string.h>

struct debug {
	uint8_t value;
	uint8_t magic;
	uint8_t tx_sleep;
	uint8_t tx_irqs;
	uint8_t rx_irqs;
	union {
		uint8_t adc_irqs;
		uint8_t rx_char;
	};
	union {
		uint8_t bate_timeout;
		uint8_t rxhex;
	};
	union {
		uint8_t spi_timeout;
		uint8_t rxpar;
	};
	uint8_t mainloops;
	uint8_t pit_irqs;
};

extern struct debug debug_data;

#define DEBUG_COUNTER(c) (debug_data.c ++)
#define DEBUG_POKE(c, v) (debug_data.c = v)

void DEBUG_PRINT(uint8_t magic, uint8_t value);
#define DEBUG_HEX(d, s) send_hex('X', (const uint8_t *)(d), s, 0)

#else // !DEBUG

#define DEBUG_COUNTER(c) 
#define DEBUG_POKE(c, v)

static inline
void DEBUG_PRINT(uint8_t magic, uint8_t value) {}

#define DEBUG_HEX(d, s) 

#endif // !DEBUG
