
#include <stdint.h>
// !!! int = int8_t

union bate {
       uint8_t b[12];
       uint16_t w[6];
       struct {
               uint16_t W[4];
               uint16_t D[2];
       };
       struct {
               uint16_t W1;
               uint16_t W2;
               uint16_t W3;
               uint16_t W4;
               uint16_t D1;
               uint16_t D2;
       };
};

struct pressure {
	uint16_t T;
	uint16_t p;
};

#define N_TESTDATA (64/sizeof(union bate))
void bate_calib(const union bate *bate, struct pressure *pt);
extern const union bate testdata[N_TESTDATA];
