#!/usr/bin/python3
# encoding: UTF-8

import sys

def calibrate(Word, D):

    print(f"""INPUT Words
W1 = 0x{Word[1]:04x}
W2 = 0x{Word[2]:04x}
W3 = 0x{Word[3]:04x}
W4 = 0x{Word[4]:04x}
D1 = 0x{D[1]:04x}
D2 = 0x{D[2]:04x}
""", file=sys.stderr)

    C=[0]*7
    C[1] = Word[1] >> 1
    C[2] = ((Word[3] & 0x3f)<<6) | (Word[4] & 0x3f) 
    C[3] = Word[4]>>6
    C[4] = Word[3]>>6
    C[5] = ((Word[1] & 1)<<10) | (Word[2]>>6)
    C[6] = Word[2] & 0x3f

    print(f"""CAL Words
C1 = {C[1]}
C2 = {C[2]}
C3 = {C[3]}
C4 = {C[4]}
C5 = {C[5]}
C6 = {C[6]}
""", file=sys.stderr)
    
    UT1 = 8*C[5]+20224
    dT = D[2] - UT1
    TEMPSENSE = C[6]+50
    TEMP = 200 + dT*TEMPSENSE//1024
    
    print(f"""Temperature
D2        = {D[2]}
UT1       = {UT1}
dT        = {dT}
TSENSENSE = {TEMPSENSE}
TEMP      = {TEMP*0.1:.1f} °C
""", file=sys.stderr)

    TCO = C[4]-512
    OFFT1 = C[2]*4 
    OFF = OFFT1 + (TCO*dT) // 4096

    SENST1 = C[1] + 24576;
    TCS = C[3]
    SENS = SENST1 + (TCS*dT) // 1024
    X = (SENS * (D[1]-7168)) // 16384 - OFF
    P = X*10//32 + 2500
    
    print(f"""Pressure
D1    = {D[1]} {D[1]-7168}
TCO   = {TCO}
OFFT1 = {OFFT1} 
OFF   = {OFF}
TCS   = {TCS}
SENST1= {SENST1}
SENS  = {SENS}  {SENS/2**14}
X     = {X}
P     = {P*0.1:.1f} mbar
""", file=sys.stderr)

    return (TEMP,P)

for l in sys.stdin:
    if l[0]!='P':
        continue
    Word=[None]
    Word.extend([int(ll,0) for ll in l.split()[1:]])
    calibrate(Word, Word[4:])
