//
// mul.c
//
// To save space in ATtinys.
// 16bit × 16bit → 16bit multiplication for AVR w/ `mul`, `muls`, `mulsu`,
// returning the high bits, not the low bits.
//
// Also provides a variant for printing decimal numbers.

#include "mul.h"

// To save space, omit what is not needed.  Use -D when needed.

#ifndef MUL_NONE
#   define MUL16SUN
#   define MUL_DECIMAL_STR
#   ifdef MUL_ALL
#       define MUL16SU
#       define MUL16SS
#       define MUL16UU
#   endif
#endif

#ifdef MUL_TEST
#define MUL_DEBUG
#include <stdio.h>
int main()
{
	int16_t tests[] = {
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		89, 99, 100, 101, 102, 0xff, 0x100, 0x101,
		999, 1000, 1001, 9999, 10000, 10001,
		0x7ffe, 0x7fff 
	};

	for (uint8_t dec = 0; dec < 5; dec++)
		for (int i=0; i < sizeof(tests)/sizeof(*tests); i++) 
			for (int s=0; s<2; s++) {
				int16_t n = tests[i];
				if (s)
					n = -n;
				printf("%u 0x%04x %+d %s\n",
				       dec, n & 0xffff, n,
				       decimal_str(n, dec));
			}
	return 0;
}
#endif

#ifndef MUL_DEBUG

#ifdef MUL16SU
int16_t mul16su(int16_t s, uint16_t u)
{
	__asm__(
		"movw  r18, %[s]"	"\n\t"
		"mul   r18, %A[u]"	"\n\t"
		"mov   r20, r1"		"\n\t"
		"mulsu r19, %B[u]"	"\n\t"
		"movw  %[s], r0"	"\n\t"
		"mul   r18, %B[u]"	"\n\t"
		"clr   r18"		"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[s], r1"	"\n\t"
		"adc   %B[s], r18"	"\n\t"
		"mulsu r19, %A[u]"	"\n\t"
		"sbc   %B[s], r18"	"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[s], r1"	"\n\t"
		"adc   %B[s], r18"	"\n\t"
		"clr   r1"		"\n\t"
		: [s] "+r" (s)
		: [u] "a" (u)
		: "r0", "r1", "r18", "r19", "r20"
		);
	return s;
}
#endif

#ifdef MUL16SUN
int16_t mul16sun(int16_t s, uint16_t u, uint8_t n)
{
	// n = 0 … 8
	__asm__(
		"movw  r18, %[s]"	"\n\t"
		"mul   r18, %A[u]"	"\n\t"
		"mov   r21, r1"		"\n\t"
		"mulsu r19, %B[u]"	"\n\t"
		"movw  %[s], r0"	"\n\t"
		"mul   r18, %B[u]"	"\n\t"
		"clr   r18"		"\n\t"
		"add   r21, r0"		"\n\t"
		"adc   %A[s], r1"	"\n\t"
		"adc   %B[s], r18"	"\n\t"
		"mulsu r19, %A[u]"	"\n\t"
		"sbc   %B[s], r18"	"\n\t"
		"add   r21, r0"		"\n\t"
		"adc   %A[s], r1"	"\n\t"
		"adc   %B[s], r18"	"\n\t"
		"cpi   %[n], 0" 	"\n\t"
		"breq  3f"		"\n\t"
		"sbrs  %[n], 3"		"\n\t"
		"rjmp  2f"		"\n\t"
		"mov   %B[s], %A[s]"	"\n\t"
		"mov   %A[s], r21"	"\n\t"
		"rjmp  3f"		"\n"
	"1:"				"\t"
		"lsl   r21"		"\n\t"
		"rol   %A[s]"		"\n\t"
		"rol   %B[s]"		"\n\t"
	"2:"				"\t"
		"dec   %[n]"		"\n\t"
		"brpl  1b"		"\n"
	"3:"				"\t"
		"clr   r1"		"\n\t"
		: [s] "+r" (s)
		: [u] "a" (u),
		  [n] "r" (n)
		: "r0", "r1", "r18", "r19", "r21"
		);
	return s;
}
#endif

#ifdef MUL16SS
int16_t mul16ss(int16_t a, int16_t b)
{
        // ((int32_t)a*b) >> 16
	__asm__(
		"movw  r18, %[a]"	"\n\t"
		"mul   r18, %A[b]"	"\n\t"
		"mov   r20, r1"		"\n\t"
		"muls  r19, %B[b]"	"\n\t"
		"movw  %[a], r0"	"\n\t"
		"mulsu %B[b], r18"	"\n\t"
		"clr   r18"		"\n\t"
		"sbc   %B[a], r18"	"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[a], r1"	"\n\t"
		"adc   %B[a], r18"	"\n\t"
		"mulsu r19, %A[b]"	"\n\t"
		"sbc   %B[a], r18"	"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[a], r1"	"\n\t"
		"adc   %B[a], r18"	"\n\t"
		"clr   r1"		"\n\t"
		: [a] "+r" (a)
		: [b] "a" (b)
		: "r0", "r1", "r18", "r19", "r20"
		);
	return a;
}
#endif

#ifdef MUL16UU
int16_t mul16uu(uint16_t a, uint16_t b)
{
        // ((uint32_t)a*(uint32_t)b) >> 16
	__asm__(
		"movw  r18, %[a]"	"\n\t"
		"mul   r18, %A[b]"	"\n\t"
		"mov   r20, r1"		"\n\t"
		"mul   r19, %B[b]"	"\n\t"
		"movw  %[a], r0"	"\n\t"
		"mul  %B[b], r18"	"\n\t"
		"clr   r18"		"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[a], r1"	"\n\t"
		"adc   %B[a], r18"	"\n\t"
		"mul   r19, %A[b]"	"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[a], r1"	"\n\t"
		"adc   %B[a], r18"	"\n\t"
		"clr   r1"		"\n\t"
		: [a] "+r" (a)
		: [b] "a" (b)
		: "r0", "r1", "r18", "r19", "r20"
		);
	return a;
}
#endif

#if defined(MUL_DIVMOD10) || defined(MUL_DECIMAL_STR)
#ifndef MUL_DIVMOD10
static inline
#endif
uint16_t divmod10(uint16_t u, uint8_t *mod)
{
	uint16_t r;
	uint8_t d; 
	__asm__(
		"ldi   r18, lo8(6554)"	"\n\t"
		"ldi   r19, hi8(6554)"	"\n\t"
		"mul   r18, %A[u]"	"\n\t"
		"mov   r20, r1"		"\n\t"
		"mul   r19, %B[u]"	"\n\t"
		"movw  %[r], r0"	"\n\t"
		"mul   r19, %A[u]"	"\n\t"
		"clr   r19"		"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[r], r1"	"\n\t"
		"adc   %B[r], r19"	"\n\t"
		"mul   r18, %B[u]"	"\n\t"
		"add   r20, r0"		"\n\t"
		"adc   %A[r], r1"	"\n\t"
		"adc   %B[r], r19"	"\n\t"
		"ldi   r19, 10"		"\n\t"
		"inc   r20"		"\n\t"
		"mul   r19, r20"	"\n\t"
		"mov   %[d], r1"	"\n\t"
		"clr   r1"		"\n\t"
		: [r] "=&r" (r),
		  [d] "=r" (d)
		: [u] "d" (u)
		: "r0", "r1", "r18", "r19", "r20"
		);
	*mod = d;
	return r;
}
#endif

#else // MUL_DEBUG

// Models for what the assembly is supposed to do.
// Also usefull for testing in non-AVR hosts.

int16_t mul16su(int16_t s, uint16_t u)
{
	uint32_t r = (int32_t)s * (int32_t)u;
	return r >> 16;
}
int16_t mul16sun(int16_t s, uint16_t u, uint8_t n)
{
	// n = 0 … 8
	uint32_t r = (int32_t)s * (int32_t)u;
	return r >> (16-n);
}
int16_t mul16ss(int16_t a, int16_t b)
{
	uint32_t r = (int32_t)a * (int32_t)b;
	return r >> 16;
}
int16_t mul16uu(uint16_t a, uint16_t b)
{
	uint32_t r = (int32_t)a * (int32_t)b;
	return r >> 16;
}
uint16_t divmod10(uint16_t u, uint8_t *mod)
{
	uint32_t r = (uint32_t)u * (uint32_t)6554;
	*mod = (((r & 0xff00)+0x100)*10) >> 16;
	return r >> 16;
}

#endif // MUL_DEBUG

#ifdef MUL_DECIMAL_STR
static uint8_t decimals[8];
char *decimal_str(int16_t b, uint8_t dec)
{
	uint8_t s = 0;
	if (b<0) {
		s = 1;
		b  = -b;
	}
	uint8_t *c = decimals+6;
	uint8_t n = 0;
	while ((b || n < dec) && n<7) {
		b = divmod10(b, c);
		*c-- += '0';
		if (++n==dec) {
			*c-- = '.';
			n++;
		}
	}
	if (!n)
		*c = '0';
	else if (s)
		*c = '-';
	else
		*c = '+';
	return (char *)c;
}
#endif
