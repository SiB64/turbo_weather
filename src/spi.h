//
// spi.h
//

#include <stdint.h>
#include <avr/io.h>

#define SPI SPI0

extern volatile uint8_t spi_tick;
uint16_t spi_frame(uint16_t d);
void init_spi(uint8_t div);
static inline void spi_off() { SPI.INTCTRL = SPI.CTRLA = 0; }
