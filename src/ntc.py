
import math

class ntc:

    """Calculate Temperature from NTC readings
    Negative Temperature Coefficient thermistors.
    The resitance follows the law of Arrhenius
    W = kβ
    R = R₂₅ exp(β/T₂₅) exp(-β/T)

    The NTC is biases via resistor R₁ from Voltage V₁.
"""
    
    β   = 3940.0 # K
    T25 = 298.16 # K
    T0  = 273.16 # K
    R25 = 10 # kΩ
    R1  = 10 # kΩ
    V1  = 1
    res = 1/0x1000

    log = math.log
    
    def Rntc(self, a, V1=None):
        """return NTC Resistance from ADC reading
        a: ADC reading,
        V1: full scale reading
"""
        if not V1:
            V1 = self.V1
        a /= V1
        if a < self.res:
            a = 1
        if a > 1 - self.res:
            a = 1 - self.res
        return self.R1 / (1/a - 1)

    def TntcR(self, R):
        """return Temperature from NTC Resistance"""
        return self.β / (self.log(R/self.R25) + self.β/self.T25) - self.T0

    def Tntc(self, a, V1=None):
        """return Temperature from ADC reading"""
        return self.TntcR(self.Rntc(a, V1))

    def TntcB(self, a, b):
        """Bridge mode
        a: bridge voltage (ADC_T - ADC_V)
        b: reference branch voltage (ADC_V)
        see `turbo.sch`
"""
        return self.Tntc(a + b, b * (1 + self.R1/self.R25))

    def R(self, T):
        """R(T)"""
        return self.R25*math.exp(self.β/(T + self.T0) - self.β/self.T25)

    def VntcT(self, T, V1=None):
        """V(T)"""
        if not V1:
            V1 = self.V1
        return V1 / (1 + self.R1/self.R(T))
