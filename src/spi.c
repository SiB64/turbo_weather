//
// spi.c
//

// !!! int = int8_t

#include "spi.h"
#include "bate.h"
#include <avr/interrupt.h>
#include <avr/sleep.h>

volatile uint8_t spi_tick;

// The Pressure sensor samples on the rising edge, MODE = 0
static const uint8_t SPI_Mode_Write = SPI_SSD_bm | SPI_BUFEN_bm;

// The Pressure sensor delivers on the rising edge, MODE = 1
static const uint8_t SPI_Mode_Read  = SPI_SSD_bm | SPI_BUFEN_bm | SPI_MODE_0_bm;

void init_spi(uint8_t div)
{
	if (div & ~SPI_PRESC_gm)
		div = SPI_PRESC_DIV64_gc;
	SPI.CTRLB = SPI_Mode_Write;
	SPI.CTRLA = SPI_MASTER_bm | SPI_ENABLE_bm | div;
	SPI.DATA;
	SPI.DATA;
	SPI.INTFLAGS = 0xff;
	SPI.INTCTRL = SPI_TXCIE_bm;
}

uint16_t spi_frame(uint16_t d)
{
	if (d)
		SPI.CTRLB = SPI_Mode_Write;
	else
		SPI.CTRLB = SPI_Mode_Read;
	SPI.DATA;
	SPI.DATA;
	SPI.INTFLAGS = 0xff;
	spi_tick = 0;
	SPI.DATA = d >> 8;
	SPI.DATA = d;
	uint8_t timeout = 20; // 20×15µs = 300µs
	sei();
	while (!spi_tick) {
		sleep_cpu();
		if (!--timeout) {
			DEBUG_COUNTER(spi_timeout);
			break;
		}
	}
	uint16_t b = SPI.DATA;
	return (b<<8) | SPI.DATA;
}

ISR(SPI0_INT_vect, ISR_NAKED)
{
	__asm__ ("push r24"          "\n\t"
		 "lds r24, %[flag]"  "\n\t"
		 "sts %[flag], r24"  "\n\t"
		 "sts spi_tick, r24" "\n\t"
		 "pop r24"           "\n\t"
		 "reti"              "\n"
		 : : [flag] "n" (&SPI.INTFLAGS)
		);
}

