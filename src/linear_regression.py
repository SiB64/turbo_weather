#! /usr/bin/python3

import sys, numpy

class linreg:

    def __init__(self, p=1, ny=1,
                 xoff=0, yoff=0,
                 decay=None, xdecay=None,
                 logscale=False):
        self.P = p
        self.p = numpy.arange(2*p+1)
        self.x = numpy.zeros((2*p+1,))
        self.y = numpy.zeros((ny, p+1))
        self.yy = numpy.zeros((ny,))
        self.xoff = xoff
        self.yoff = yoff
        self.decay = decay
        self.xdecay = xdecay
        self.N = 0
        self.logscale = logscale

        Ai = numpy.arange(p+1)
        self.Ai = Ai+Ai.reshape((-1,1))

        if xoff is True:
            self.Bi = self.pascal()

    def pascal(self):
        p = self.p.shape[0]
        Bi = numpy.zeros((p,p,p), dtype=int)
        for n in range(p):
            Bi[n,0,n] = 1
            Bi[n,n,0] = 1
        for n in range(1,p):
            for k in range(1,n):
                Bi[n,k,n-k] = Bi[n-1,k-1,n-k] + Bi[n-1,k, n-k-1]
        return Bi

    def add(self, x, y, w=1.0, decay=None):
        
        y = numpy.array(y, dtype=float).reshape((-1,))
        if self.logscale:
            y = numpy.log(y)

        if decay is None:
            if self.N and self.xdecay is not None:
                if x <= self.lastx:
                    decay = 1
                    self.N = 0
                else:
                    decay = 1 - numpy.exp((self.lastx - x)/self.xdecay)
            else:
                decay  = self.decay
        self.lastx = x

        if decay is not None:
            self.x *= 1 - decay
            self.y *= 1 - decay
            self.yy *= 1 - decay

        if self.xoff is True:
            if self.N:
                self.transform_x(x - self.x_origin)
            self.x_origin = x
            if self.yoff is True:
                if self.N:
                    self.transform_y(y - self.y_origin)
                self.y_origin = y
            else:
                self.y[:, 0] += w * y
                self.yy += w * y*y
            self.x[0] += w
            self.N += 1 
            return
            
        self.N += 1
        x -= self.xoff
        y -= self.yoff
        x = w * numpy.power(x, self.p)
        self.x += x
        self.y += y[:,numpy.newaxis] * x[:self.P+1]
        self.yy += w * y*y
        
    def solve(self):
        if self.N <= self.P:
            return
        try:
            return numpy.linalg.solve(self.x[self.Ai], self.y[...,numpy.newaxis])[...,0]
        except numpy.linalg.LinAlgError:
            print(self.N, self.x[self.Ai], self.y, file=sys.stderr)

    def solve_p(self, p):
        if self.N <= p or p > self.P:
            return
        Ai = self.Ai[:p+1, :p+1]
        y = self.y[:, :p+1, numpy.newaxis]
        try:
            return numpy.linalg.solve(self.x[Ai], y)
        except numpy.linalg.LinAlgError:
            print(self.x[Ai], y, file=sys.stderr)

    def transform_x(self, x):
        p = numpy.power(-x, self.p).reshape((-1,1))
        Bi = self.Bi @ p
        xx = self.x[numpy.newaxis,:] @ Bi
        self.x = xx[:,0,0]
        yy = self.y[:,numpy.newaxis,numpy.newaxis,:] @ Bi[:self.P+1,:self.P+1]
        self.y = yy[...,0,0]

    def transform_y(self, y):
        self.yy += self.x[0]*y*y - 2*y*self.y[:,0]
        self.y -= y[:,numpy.newaxis] * self.x[:self.P+1]
        
    def print_result(self, r, fmt="%.12g"):
        if r is None:
            print("")
            return
        dx = self.xoff
        dy = self.yoff
        if dx is True:
            dx = self.x_origin
            if dy is True:
                dy = self.y_origin
        r[:,0] += dy
        if self.logscale:
            r[:,0] = numpy.exp(r[:,0])
            dy = numpy.exp(dy)
        dy += numpy.zeros(self.yy.shape)
        for i, rr in enumerate(r):
            print(i, fmt % dx, fmt % dy[i], " ".join([fmt % rrr for rrr in rr]))

def strtonum(s):
    try:
        return float(s)
    except:
        pass
    return int(s, 0)

def main():

    import sys, getopt, fileinput
    options, files = getopt.gnu_getopt(sys.argv[1:], "lp:n:x:y:w:d:X:Y:D:T:I")
    p = 1
    ny = 1
    ix = 0
    iy = 1
    iw = None
    id = None
    w = 1.0
    d = None
    I = False
    args = {}
    for o, v in options:
        if o=="-p":
            args["p"] = int(v)
        if o=="-n":
            ny = int(v)
            args["ny"] = ny
        if o=="-X":
            if v=="R":
                args["xoff"] = True
            else:
                args["xoff"] = int(v)
        if o=="-Y":
            if v=="R":
                args["xoff"] = True
                args["yoff"] = True
            else:
                args["yoff"] = int(v)
        if o=="-D":
            args["decay"] = float(v)
        if o=="-T":
            args["xdecay"] = float(v)
        if o=="-l":
            args["logscale"] = True
        if o=="-x":
            ix = int(v)
        if o=="-y":
            iy = int(v)
        if o=="-w":
            iw = int(v)
        if o=="-d":
            id = int(v)
        if o=="-I":
            I = True
            
    LR = linreg(**args)

    for l in fileinput.input(files):
        ll = l.split()
        try:
            x = strtonum(ll[ix])
            y = numpy.array([strtonum(lll) for lll in ll[iy:iy+ny]])
            if iw is not None:
                w = strtonum(ll[iw])
            if id is not None:
                d = strtonum(ll[id])
        except:
            continue
        LR.add(x, y, w, d)
        if I:
            LR.print_result(LR.solve())

    if not I:
        LR.print_result(LR.solve())

if __name__=="__main__":
    main()
