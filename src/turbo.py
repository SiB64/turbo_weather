#! /usr/bin/python3

import sys, time, getopt, serial, fileinput
import pressure, ntc, linear_regression, cmdsocket

options, files = getopt.gnu_getopt(sys.argv[1:], "xF:s:o:c", ["debug", "tty=", "noise", "clock", "socket=", "output="])

tty = None
baud = 2400
socket = None
out = None

do_noise = False
do_clock = False
debug = False

def Debug(e, *a):
    if debug:
        import traceback
        sys.stdout.flush()
        print("xdebug", a, repr(e), file=sys.stderr)
        traceback.print_exception(e, limit=-2, file=sys.stderr)

for o,v in options:

    if o == "--debug":
        debug = True

    if o in "-F --tty":
        if tty:
            raise ValueError("can only do one tty")
        tty = v
        v = v.split(",", 1)
        if v[1:]:
            tty = v[0]
            baud = int(v[1])
        do_clock = True

    if o in "--clock":
        do_clock = True

    # BUG: matches -n
    if o in "-x --noise":
        do_noise = True

    if o in "--socket":
        socket = v

    if o in "--output":
        if out:
            raise ValueError("cannot have multiple --outputs")
        if v=="-":
            out = sys.stdout
        elif v=="--":
            out = sys.stderr
        else:
            out = open(v, "a")

if not out:
    out = sys.stdout

if len(files)==1:
    if "/dev/tty" in files[0]:
        tty = files[0]
        files = []

inp = []
if tty:
    inp = [serial.Serial(port=tty, baudrate=baud)]
if files or not inp:
    inp[0:0] = [fileinput.input(files, mode="rb")]

checksum = 0

def add_checksum(line):
    global checksum
    checksum += sum(line)
    # noise(line, "s")

def noise(line, prefix="x"):
    if not do_noise:
        return False
    trunc = ""
    print(prefix, repr(line), file=out)
    return False

def echo(line, *a):
    try:
        print(line.strip().decode(), *a, file=out)
        return True
    except:
        pass
    return noise(line)

def check_sum(line):
    global checksum
    try:
        rcs = int(line.split()[0][1:], 16)
    except:
        return noise(line)
    if rcs > 0xff:
        return noise(line)
    add_checksum(line[:1])
    lcs = checksum & 0xff
    checksum = 0

    if lcs==rcs:
        echo(line, "✓")
        emit()
    else:
        echo(line, f"{lcs:02x}", "Checksum Error")

    return False

Values = {}

NTC = ntc.ntc()

def voltages(line):
    ll = line.split()
    try:
        vv = { 
            "Tcpu":  float(ll[1]),
            "Vcpu":  float(ll[2]),
            "Vbat":  float(ll[3]),
            "dVntc": float(ll[4]),
            "Vntc":  float(ll[5]),
            "cVntc": int(ll[6], 16),
            "Vrf":   float(ll[7]),
            "cVrf":  int(ll[8], 16),
        }
    except:
        return noise(line)

    Values.update(vv)
    emit_voltages(**vv)

    return echo(line)

def emit_voltages(Tcpu, Vcpu, Vbat, dVntc, Vntc, cVntc, Vrf, cVrf):
    Tntc = NTC.TntcB(dVntc, Vrf)
    Tntc2 = NTC.TntcB(cVntc-cVrf, cVrf)
    print(f"v {Tcpu:.1f}°C"
          f" {Tntc:.2f}°C"
          f"  Bat {Vbat:.3f}V"
          f"  Vcc {Vcpu:.3f}V"
          f"  Vrf {Vrf/500:.3f}V",
          file=out)

freq = linear_regression.linreg(p=2, xoff=True, yoff=True, xdecay=600)

def clock(line):
    ll = line.split()
    try:
        c = int(ll[1], 16)
    except:
        return noise(line, "t")
    s = None
    t = None
    if do_clock:
        t = time.time()
    try:
        t = float(ll[3])
    except:
        pass
    if t is not None:
        freq.add(t, c)
        s = freq.solve()
    if s is None:
        if t is None:
            t = time.time()
        return echo(line, f"{c} {t:.1f}")
    return echo(ll[0]+b' '+ll[1], f"{c} {t:.1f}", *("%.6g" % ss for ss in s[0]))

Data = {}

def data(line):
    ll = line.split()
    try:
        c = ll[0]
        d = [int(l, 16) for l in ll[1:]]
        if ll[1:]:
            Data[c] = d
            emit_data(c)
            return echo(line)
    except Exception as e:
        Debug(e, line)
    return noise(line, "h")

def sigrow(line):
    try:
        c = line[:1]
        Data[c] = [int(line[i:i+2], 16) for i in range(1,len(line.strip()),2)]
        emit_data(c)
        return echo(line)
    except Exception as e:
        Debug(e, line)
    return noise(line, "h")

def emit_data(c):
    if c not in emitters:
        return
    try:
        return emitters[c](c, Data[c])
    except Exception as e:
        Debug(e, c)

def emit_pressure(c, cc):
    W = [None]
    D = [None]
    W.extend(Data[b'W'])
    D.extend(Data[b'D'])
    if len(W) != 5 or len(D) != 3:
        return
    try:
        T, p = pressure.calibrate(W, D)
    except Exception as e:
        print(f"pressure.calibrate failed {e}", file=sys.stderr)
    print(f"p {p/10:.2f} mbar, {T/10:.2f} °C")

from cmd import turbocmd

millivolts = {
    "VDD":  1000,
    "1V":   1024,
    "2V":   2048,
    "2.5V": 2500,
    "4V":   4096,
}

ADC_mV = {}

def emit_adc(c, cc):
    if len(cc) > 8:
        return
    ADC_mV.clear()
    for n, a in enumerate(cc):
        C = ADC_CONFIG[n]
        if C is None:
            continue
        if C["mode"] == turbocmd.ADC_MODE["DIFF"]:
            if a & 0x8000:
                a -= 0x10000
            a *= 2
        elif C["mode"] != turbocmd.ADC_MODE["NORM"]:
            continue
        REF = None
        for k, i in turbocmd.ADC_REF.items():
            if C["ref"] == i:
                REF = k
                mV = millivolts[k]
        if REF is None:
            continue
        A = a * mV / 0x10000
        inp = "?"
        inn = ""
        for k, i in turbocmd.ADC_INP.items():
            if C["inp"] == i:
                inp = k
            if C["mode"] == turbocmd.ADC_MODE["DIFF"]:
                if C["inn"] == i:
                    inn = "-"+k
        inp = inp+inn+"/"+REF
        ADC_mV[inp] = (A, mV)
    emit_adc_pretty()

def emit_adc_pretty():
    for k, a in ADC_mV.items():
        A, mV = a
        units = "V ref"
        pretty = ""
        kk = k.split("/")
        if kk[0] == "TEMP":
            try:
                T = (A/mV*0x10000 - TEMP_CAL["offset"]) * TEMP_CAL["gain"] - 273.16
                pretty = f" Tcpu {T:.2f} °C"
            except Exception as e:
                Debug(e, k)
        if kk[0] == "NTC":
            try:
                AA, x = ADC_mV["RFP/"+kk[1]]
                T = NTC.TntcB(A-AA, AA)
                pretty = f" Tntc {T:.2f} °C"
            except Exception as e:
                Debug(e, k)
        if kk[0] == "NTC-RFP":
            try:
                AA, x = ADC_mV["RFP/2.5V"]
                T = NTC.TntcB(A, AA)
                pretty = f" Tntc {T:.2f} °C"
            except Exception as e:
                Debug(e, k)
        if kk[0] == "VDD":
            pretty = f" Vdd  {A/100:.4f} V"
        if kk[0] == "BAT":
            pretty = f" Vbat {A/1000*11:.4f} V"
        if k == "RFP/2.5V":
            pretty = f" Vrf  {A/500:.4f} V"
        if kk[1] == "VDD":
            units=". Vdd"
            try:
                AA, x = ADC_mV["VDD/1V"]
                mV = AA*10
                pretty += f" {kk[0]}  {A*mV/1e6:.4f} V"
            except Exception as e:
                Debug(e, k)
            try:
                AA, x = ADC_mV[kk[0]+"/2.5V"]
                pretty += f" Vdd {AA/A:.4f} V"
            except Exception as e:
                Debug(e, k)

        print(f"a {k:10} {A/1000:.5f} {units} {mV/1000:.3f} V{pretty}")

def emit_configuration(c, cc):
    c = c.lower().decode()
    if len(cc) != 32:
        return
    for r,v  in turbocmd.REGS.items():
        a = v[0]
        b = cc[a]
        if v[1] == 2:
            b |= cc[a+1] << 8
        if len(v) >= 3:
            bb = []
            for k,i in v[2].items():
                if not ~b & i:
                    bb.append(k)
            print(c, r, bb)
            continue
        if v[1] < 3:
            print(c, r, "%02x" % b)
            continue
        print(c, r, " ".join([f"0x{b:02x}" for b in cc[a:a+v[1]]]))

ADC_CONFIG = [None]*8


def emit_eeprom(c, cc):
    if len(cc) < 32:
        return
    for n in range(8):
        bb = [""]*7
        aa = {}
        for r,v  in turbocmd.ADC_CONF.items():
            a = 8*n + v[0]
            b = cc[a//2]
            if v[1]==1:
                if a&1:
                    b >>= 8
                b &= 0xff
            bb[v[0]] = f"{b}"
            aa[r] = b
            for k, i in v[2].items():
                if b==i:
                    bb[v[0]] = k
        ADC_CONFIG[n] = aa
        print("e", n, " ".join(bb))

SIGROW = {
    "DEVID":    (0,    3),
    "SERNO":    (3,   10),
    "OSCCAL":   (0x18, 4),
    "TEMPSENS": (0x20, 2),
}

DEVID = {
    0x1e922a: "ATtiny427",
    0x1e922b: "ATtiny426",
    0x1e922c: "ATtiny424",
    0x1e9327: "ATtiny827",
    0x1e9328: "ATtiny826",
    0x1e9329: "ATtiny824",
}

def devid():
    cc = Data[b'S'][2::-1]
    return sum([b << (8*i) for i,b in enumerate(cc)])

TEMP_CAL = {"gain": 0, "offset": 0}

def emit_sigrow(c, cc):
    if len(cc)<34:
        return
    for r,v in SIGROW.items():
        print(f"s {r} {' '.join([f'{i:02x}' for i in cc[v[0]:v[0]+v[1]]])}")
    i = devid()
    if i in DEVID:
        print(f"s DEVICE 0x{i:06x} {DEVID[i]} SN {bytes(cc[3:13])}")
    v = SIGROW["TEMPSENS"]
    g = cc[v[0]]
    o = cc[v[0]+1]
    if o & 0x80:
        o -= 0x100
    o <<= 6
    g /= 1 << (6+8)
    TEMP_CAL.update({"gain": g, "offset": o})
    print(f"s ADC_CAL offset {o} gain {g:.7f}")

FUSES = {
    "WDT": (0, {"PERIOD": (0,0xf), "WINDOW": (4,0xf)}),
    "BOD": (1, {"SLEEP": (0,3), "ACTIVE": (2,3), "SAMPFREQ": (4,1), "LVL": (5,7)}),
    "OSC": (2, {"OSCLOCK": (7,1), "FREQSEL": (0,1)}),
    "SYS0": (5, {"EESAVE": (0,1), "RSTPIN": (2,3), "TOUTDIS": (4,1), "CRCSRC": (6,3)}),
    "SYS1": (6, {"SUT": (0,7)}),
    "APPEND": (7, {}),
    "BOOTEND": (8, {}),
}

def emit_fuses(c, cc):
    if len(cc) != 9:
        return
    for r,v in FUSES.items():
        b = cc[v[0]]
        bb = f"f {v[0]} {r} 0x{b:02x}"
        for k,i in v[1].items():
            bb += f" {k}={(b >> i[0]) & i[1]}"
        print(bb)

emitters = {
    b'C': emit_configuration,
    b'U': emit_configuration,
    b'E': emit_eeprom,
    b'S': emit_sigrow,
    b'F': emit_fuses,
    b'D': emit_pressure,
    b'A': emit_adc,
}

def resp(line):
    ll = line.split()
    if line[1] not in b'!?>':
        return noise(line)
    echo(line)
    checksum = 0
    return False

def emit():
    pass

processes = {
    b'A': data,
    b'B': echo,
    b'C': data,
    b'D': data,
    b'E': data,
    b'F': data,
    b'P': echo,
    b'Q': check_sum,
    b'R': resp,
    b'S': sigrow,
    b'T': clock,
    b'U': data,
    b'V': voltages,
    b'W': data,
    b'X': echo,
}

splits = {
    b'Q': 1,
    b'T': 2,
}

class batecmd(cmdsocket.cmd_receiver):
    def msg(self, m):
        echo(b'r '+m)
        inp.write(b'\n' + m + b'\n')

cmd = batecmd(path=None)
if socket:
    if not tty:
        raise ValueError("cannot have socket without tty")
    cmd.open(socket, force=True)

while inp:
    try:
        line = inp[0].readline()
    except KeyboardInterrupt:
        break
    if not line:
        inp[:1] = []
        continue

    line_key = line[0:1]
    is_noise = line_key not in processes
    if not is_noise and min(line.strip()) < 32:
        is_noise = True

    if line_key in b"apcufse":
        continue
    if is_noise:
        if do_noise:
            noise(line)
        check_sum = 0
        continue

    if processes[line_key](line):
        if line_key in splits:
            line = b" ".join(line.split()[:splits[line_key]])+b"\n"
        add_checksum(line)

    if tty:
        out.flush()

    cmd.poll()
