//
// bate.c
//

// !!! int = int8_t

#include <stdint.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#include "bate.h"
#include "rtc.h"
#include "calib.h"
#include "mul.h"
#include "adc.h"
#include "spi.h"

#define Bit(x) (1<<(x))

////////////////////////////////////////////////////////////////////////////////
//
//  LED on PA5

#define LED_P VPORTA
#define LED_B Bit(5)

static inline
void init_led()
{
	LED_P.DIR |= LED_B;
}

static inline
void led(uint8_t on)
{
	if (on)
		LED_P.OUT |= LED_B;
	else
		LED_P.OUT &=~ LED_B;
}

////////////////////////////////////////////////////////////////////////////////
//
//  RFEN on PB1, enable the LDO to power the RF transmitter

#define RFEN_P VPORTB
#define RFEN_B Bit(1)

static inline
void init_rfen()
{
	RFEN_P.DIR |= RFEN_B;
}

static inline
void rfen(uint8_t on)
{
	if (on)
		RFEN_P.OUT |= RFEN_B;
	else
		RFEN_P.OUT &=~ RFEN_B;
}

static inline
uint8_t rfen_status()
{
	return RFEN_P.OUT & RFEN_B;
}

/////////////////////////////////////////////////////////////////////////////
//
// BATE, read data from an MS5534C pressure sensor

#define BATE_PORT VPORTA
#define SCK_PORT  3
#define DOUT_PORT 2
#define DIN_PORT  1

// MCLK timer.
//
// The nominal f_{clk_per} is 10 MHz,
// f_{MCLK} needs to be 32768 Hz
// f_{tick} needs to be 2×f_{MCLK}
// Timer 1 TOP needs to be f_{clk_per}/65536

#ifndef PERIOD
#    define PERIOD 76
#endif

// user TCA0 in 16 bit mode.  SPLIT mode is also viable.

#define MCLK TCA0.SINGLE

// The CMP0 port seems to be randomly high after TCA disable
// The pin on the sensor draws  about 30µA when high.
// Just toggling the ENABLE bit is insufficient.

static inline
void mclk(uint8_t on)
{
	if (on) {
		MCLK.CTRLB = TCA_SINGLE_CMP0EN_bm | TCA_SINGLE_WGMODE_FRQ_gc;
		MCLK.CTRLA = TCA_SINGLE_CLKSEL_DIV2_gc | TCA_SINGLE_ENABLE_bm;
	}
	else {
		MCLK.CTRLA = 0;
		MCLK.CTRLB = 0;
	}
}

static inline
void init_mclk(uint8_t p)
{
	if (!p || p==0xff)
		p = PERIOD;
	VPORTB.DIR |= Bit(0); // MCLK
	MCLK.INTCTRL = TCA_SINGLE_CMP0_bm;
	MCLK.CMP0 = p;
	mclk(1);
	BATE_PORT.DIR |= Bit(DIN_PORT);
	BATE_PORT.DIR |= Bit(SCK_PORT);

	// DOUT input.
	// The pullup draws 80µA in STDBY, when the sensor happens
	// to end with DOUT=0.  The attached logic analyser drew 30µA,
	// when DOUT=1 so the STDBY current was toggling between 50µA and 100µA.
	// 20µA goes into the LDO.  Which we may want to remove.

#   ifdef DOUT_PULLUP
#       define PA2_PULLUP PORT_PULLUPEN_bm
	PORTA.PIN2CTRL = PA2_PULLUP;
#   else
#       define PA2_PULLUP 0
#   endif

}

ISR(PORTA_PORT_vect, ISR_NAKED)
{
	__asm__ ("push r24"         "\n\t"
		 "lds r24,%[stat]"  "\n\t"
		 "sts %[stat], r24" "\n\t"
		 "pop r24"          "\n\t"
		 "reti"             "\n"
		 : : [stat] "n" (&VPORTA.INTFLAGS)
		);
}

volatile uint8_t tick;

ISR(TCA0_CMP0_vect, ISR_NAKED)
{
	__asm__ ("push r24"         "\n\t"
		 "ldi r24, 0x10"    "\n\t"
		 "sts %[flag], r24" "\n\t"
		 "sts tick, r24"    "\n\t"
		 "pop r24"          "\n\t"
		 "reti"             "\n"
		 :
		 : [flag] "n" (&MCLK.INTFLAGS)
		);
}

__attribute__ ((noinline, noclone))
static
void bate_wait()
{
	uint16_t timeout = 3277; // 50ms
	tick = 0;
	while (BATE_PORT.IN & Bit(DOUT_PORT)) {
		PORTA.PIN2CTRL = PA2_PULLUP | PORT_ISC_FALLING_gc;
		sleep_cpu();
		if (tick) {
			tick = 0;
			if (!--timeout) {
				DEBUG_COUNTER(bate_timeout);
				break;
			}
		}
	}
	PORTA.PIN2CTRL = PA2_PULLUP | PORT_ISC_INTDISABLE_gc;
}

static inline
uint8_t mclk_status()
{
	return MCLK.CTRLA & TCA_SINGLE_ENABLE_bm;
}

union bate bate;
struct pressure pressure;

#ifdef BIT_BANG_BATE

// Spec:
//            _____       _____       _____
//  SCK  ____/     \_____/     \_____/     \_____
//         _____       _____       _____       __
//  DIN  -<_____>-----<_____>-----<_____>-----<__
//       _____ ___________ ___________ __________
//  DOUT _____X___________X___________X__________
//
// Impl:
//               _______
//  SCK  _______/       \_
//       _ _______________
//  DIN  _X_______________
//
//  DOUT xxxxxSxxxxxxxxxxx
//

__attribute__ ((noinline, noclone))
static
uint8_t bate_bit(uint8_t r, uint8_t c, uint8_t ii)
{
#if 0
	uint8_t clkl = 10;
	uint8_t clkh = 30;
	if (c & ii)
		BATE_PORT.OUT |= Bit(DIN_PORT);
	else
		BATE_PORT.OUT &=~ Bit(DIN_PORT);
	if (BATE_PORT.IN & Bit(DOUT_PORT))
		r |= ii;
	else
		r &= ~ii;
	while(clkl)
		clkl--;
	BATE_PORT.OUT |= Bit(SCK_PORT);
	while(clkh)
		clkh--;
	BATE_PORT.OUT &=~ Bit(SCK_PORT);
#else
	__asm__(
		"and	%[c], %[ii]"		"\n\t"
		"brne 1f"			"\n\t"
		"cbi	%[PORT], %[DIN]"	"\n\t"
	"1:"					"\n\t"
		"breq 2f"			"\n\t"
		"sbi	%[PORT], %[DIN]"	"\n\t"
	"2:"					"\n\t"
		"sbic	%[PIN], %[DOUT]"	"\n\t"
		"or	%[r], %[ii]"		"\n\t"
		"com	%[ii]"			"\n\t"
		"sbis	%[PIN], %[DOUT]"	"\n\t"
		"and	%[r], %[ii]"		"\n\t"

		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"

		"sbi	%[PORT], %[SCK]"	"\n\t"

		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"
		"nop"				"\n\t"

		"cbi	%[PORT], %[SCK]"	"\n"
		
		:[r]   "+r" (r),
		 [c]   "+r" (c)
		:[ii]  "r" (ii),
		 [PORT] "n" (_SFR_IO_ADDR(BATE_PORT.OUT)),
		 [PIN]  "n" (_SFR_IO_ADDR(BATE_PORT.IN)),
		 [DIN]  "n" (DIN_PORT),
		 [DOUT] "n" (DOUT_PORT),
		 [SCK]  "n" (SCK_PORT)
		: "memory"
		);
#endif
	return r;
}

__attribute__ ((noinline, noclone))
static
uint16_t bate_frame(uint16_t d, uint8_t n)
{
	n--;
	uint8_t c0 = d;
	uint8_t c1 = d >> 8;
	uint8_t r0;
	uint8_t r1;
	// Force r0 and r1 to be cleared before the switch.  Else, the
	// compiler generates extra jumps to clear r0 for each target.
	__asm__("clr %[r0]"   "\n\t"
		"clr %[r1]"   "\n"
		:[r0] "=r" (r0),
		 [r1] "=r" (r1)
		);
	switch (n) {
	case 15: r1 = bate_bit(r1, c1, 1<<7);
	case 14: r1 = bate_bit(r1, c1, 1<<6);
	case 13: r1 = bate_bit(r1, c1, 1<<5);
	case 12: r1 = bate_bit(r1, c1, 1<<4);
	case 11: r1 = bate_bit(r1, c1, 1<<3);
	case 10: r1 = bate_bit(r1, c1, 1<<2);
	case  9: r1 = bate_bit(r1, c1, 1<<1);
	case  8: r1 = bate_bit(r1, c1, 1<<0);
	case  7: r0 = bate_bit(r0, c0, 1<<7);
	case  6: r0 = bate_bit(r0, c0, 1<<6);
	case  5: r0 = bate_bit(r0, c0, 1<<5);
	case  4: r0 = bate_bit(r0, c0, 1<<4);
	case  3: r0 = bate_bit(r0, c0, 1<<3);
	case  2: r0 = bate_bit(r0, c0, 1<<2);
	case  1: r0 = bate_bit(r0, c0, 1<<1);
	case  0: r0 = bate_bit(r0, c0, 1<<0);
	}
	return (uint16_t)r1<<8 | r0;
}

static inline
void read_bate()
{
	cli();
	bate_frame(0xaaaa, 16);
	bate_frame(0, 8);
	bate_frame(0x3aa0, 16);
	bate.W1 = bate_frame(0, 16);
	bate_frame(0x3ac0, 16);
	bate.W2 = bate_frame(0, 16);
	bate_frame(0x3b20, 16);
	bate.W3 = bate_frame(0, 16);
	bate_frame(0x3b40, 16);
	bate.W4 = bate_frame(0, 16);
	bate_frame(0x0f40, 16);
	sei();
	bate_wait();
	cli();
	bate_frame(0,1);
	bate.D1 = bate_frame(0, 16);
	bate_frame(0x0f20, 16);
	sei();
	bate_wait();
	cli();
	bate_frame(0,1);
	bate.D2 = bate_frame(0, 16);
	sei();
}

#else // !BIT_BANG_BATE

static inline
void read_bate()
{
	init_spi(config.spi_div);
	spi_frame(0b0000000010101010);
	spi_frame(0b1010101000000000);
	spi_frame(0b0001110101010000);
	bate.W1 = spi_frame(0);
	spi_frame(0b0001110101100000);
	bate.W2 = spi_frame(0);
	spi_frame(0b0001110110010000);
	bate.W3 = spi_frame(0);
	spi_frame(0b0001110110100000);
	bate.W4 = spi_frame(0);
	spi_frame(0b0000111101000000);
	bate_wait();
	bate.D1 = spi_frame(0);
	spi_frame(0b0000111100100000);
	bate_wait();
	bate.D2 = spi_frame(0);
	spi_off();
}

#endif // !BIT_BANG_BATE

////////////////////////////////////////////////////////////////////////////////
//
//  Configuration in USERROW

struct config config = {
	.magic        = USE_USERROW,
	.version      = USE_VERSION,
	.triggers     = TRIGGER_CLOCK | TRIGGER_IMMED,
	.send         = SEND_CONFIG | SEND_CLOCK | SEND_BATED | SEND_ADC_HEX,
	.power        = STOP_MCLK | POWER_STDBY | POWER_LED | POWER_RF | POWER_LINE,
	.calib_test   = 0,
	.spi_div      = SPI_PRESC_DIV64_gc,
	.mclk_delay   = 2,
	.period       = 9,
	.confp        = 30,
	.cpu_clk      = CLKCTRL_PDIV_2X_gc | 1,
	.mclk_period  = 0,
	.baud_div     = 0,
	.uart_mode    = UART_Tx | UART_Rx | UART_SFD,
	.pit_period   = RTC_CLKSEL_INT1K_gc,
	.immediate    = 5,
	.pad          = {},
	.line_preable = "\xff\xff\xff\xff\xff\x55\n"
};

uint8_t immediate;
uint8_t test_calib;

static const struct config *userrow = (struct config *) & USERROW;

#ifdef DEBUG
struct debug debug_data, debug_data_copy;
void DEBUG_PRINT(uint8_t magic, uint8_t value)
{
	cli();
	memcpy(&debug_data_copy, &debug_data, sizeof(debug_data));
	memset(&debug_data, 0, sizeof(debug_data));
	sei();
	debug_data_copy.magic = magic;
	debug_data_copy.value = value;
	send_hex('X', &debug_data_copy.value, sizeof(struct debug), 3);
}

#endif

////////////////////////////////////////////////////////////////////////////////
//
//  main()

int main()
{
	if (userrow->magic == USE_USERROW
	    && userrow->version == USE_VERSION)
		memcpy(&config, userrow, sizeof(struct config));

	while (CLKCTRL.MCLKCTRLB != config.cpu_clk) {
		CCP = CCP_IOREG_gc;
		CLKCTRL.MCLKCTRLB = config.cpu_clk;
	}

	init_adc();
	init_mclk(config.mclk_period);
	init_led();
	init_rfen();
	init_uart(config.baud_div, config.uart_mode);
	init_rtc(config.pit_period);

	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
	sei();

	uint8_t reset_source = RSTCTRL.RSTFR;
	RSTCTRL.RSTFR = reset_source;
	send_str("\nB Turbo Weather V0.9 ");
	send_hex_byte(reset_source);
	send_eol();

	uint8_t n_adc = 0;
	while (n_adc < N_ADC && adc_conf[n_adc].mode && adc_conf[n_adc].mode != 0xff)
		n_adc++;

	uint8_t trigger = TRIGGER_CONT | TRIGGER_ONCE;
	uint8_t mclk_delay = 0;
	uint8_t trigger_clock = 0;
	uint8_t config_clock = 0;
	uint8_t send_config = 0;
	uint8_t uart_rx_delay = 0;
	immediate = config.immediate;
	test_calib = config.calib_test;

	while (1) {
		DEBUG_COUNTER(mainloops);
		sleep_cpu();
		set_sleep_mode(SLEEP_MODE_IDLE);

		if (clock_tick) {
			clock_tick = 0;
			if (!trigger_clock--) {
				trigger |= TRIGGER_CLOCK;
				trigger_clock = config.period;
			}
			if (uart_rx_delay)
				uart_rx_delay--;
		}

		if (config.send & SEND_ADC  &&  adc_current <= N_ADC) {
			if (adc_busy())
				continue;
			adc_current = 0xff;
			if (config.send & SEND_ADC_HEX)
				send_hex('A', (uint8_t *)adc_readings,
					 2*n_adc, 3);

			if (config.send & SEND_ADC_VOLT) {
				send_char('V');
				for (uint8_t i=0; i<n_adc; i++)
					send_calib_adc(i);
				send_eol();
			}
			if (config.power & POWER_LINE) {
				send_cks();
				uart_cks = 0;
			}
		}

		command();

		if (uart_busy()) {
			uart_tick();
			continue;
		}

		wdt_reset();

		if (uart_tick()) {
			trigger |= TRIGGER_UART;
			// We woke up for a blibb on Rx.
			// Keep the power up for this second,
			// to give the SFD a chance.
			if (config.power & POWER_RX)
				uart_rx_delay = 2;
		}

		if (uart_break_p())
			trigger |= TRIGGER_BREAK;

		if (immediate || test_calib)
			trigger |= TRIGGER_IMMED;

		if (!(trigger & config.triggers)) {
			rfen(0);
			led(0);
			if (config.power & (POWER_DOWN|POWER_STDBY)) {
				mclk(0);
				if (config.power & POWER_DOWN && !uart_rx_delay) {
					if (config.power & POWER_DOWN_CLI)
						cli();
					set_sleep_mode(SLEEP_MODE_PWR_DOWN);
				}
				else
					set_sleep_mode(SLEEP_MODE_STANDBY);
			}
			continue;
		}

		if (!rfen_status()) {
			if (config.power & POWER_RF)
				rfen(1);
			if (config.power & POWER_LINE) {
				send_str(config.line_preable);
				uart_cks = 0;
			}
		}
		if (config.power & POWER_LED)
			led(1);

		if (!mclk_status())
			mclk_delay = config.mclk_delay;
		mclk(1);

		if (!tick)
			continue;
		tick = 0;

		if (mclk_delay) {
			mclk_delay--;
			continue;
		}

		if (config.send & SEND_CONFIG && !config_clock--) {
			send_config = 1;
			config_clock = config.confp;
		}

		if (immediate)
			immediate--;

		read_bate();
		if (config.power & STOP_MCLK)
			mclk(0);

		if (config.send & SEND_ADC)
			start_adc();

		if (send_config) {
			send_hex('S', (uint8_t *)&SIGROW, sizeof(SIGROW_t), 0);
			send_hex('F', (uint8_t *)&FUSE, sizeof(FUSE_t), 1);
			send_hex('U', (uint8_t *)&USERROW, sizeof(USERROW_t), 1);
			send_hex('C', (uint8_t *)&config, sizeof(struct config), 1);
			send_hex('E', (uint8_t *)adc_conf, sizeof(adc_conf), 3);
		}

		if (config.send & SEND_CLOCK) {
			send_str("T 0x");
			cli();
			uint32_t time = clock;
			sei();
			send_hex_long(time);
			send_eol();
		}

		uint8_t send_test = 0;
		const union bate *b = &bate;
		if (test_calib) {
			if (test_calib > N_TESTDATA)
				test_calib = N_TESTDATA;
			b = testdata + -- test_calib;
			send_test = 1;
			send_config = 1;
		}
		if (config.send & SEND_BATEW || send_config)
			send_hex('W', b->b, 8, 3);
		if (config.send & SEND_BATED || send_config)
			send_hex('D', b->b+8, 4, 3);
		if (config.send & SEND_CALIB || send_test) {
			bate_calib(b, &pressure);
			send_str("P ");
			send_str(decimal_str(pressure.p, 1));
			send_str(" mbar, ");
			send_str(decimal_str(pressure.T-2732, 1));
			send_str(" °C\n");
		}

		if (config.send & SEND_DEBUG) {
			DEBUG_PRINT(0x77, trigger);
		}

		if (config.power & POWER_LINE) {
			send_cks();
			uart_cks = 0;
		}

		uart_tick();
		trigger = TRIGGER_CONT;
		send_config = 0;
	}
}
