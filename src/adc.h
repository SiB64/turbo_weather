//
// adc.h
//

#include <avr/io.h>
#define ADC ADC0

struct adc_conf {
	uint8_t mode;
	uint8_t ref;
	uint8_t inp;
	uint8_t inn;
	int8_t offset;
	uint8_t shifts;
	uint16_t calib;
};

#define N_ADC (64/sizeof(struct adc_conf))

extern uint16_t adc_readings[N_ADC];
extern uint8_t adc_current;

void start_adc();
void init_adc();
static inline uint8_t adc_busy() { return ADC.STATUS & 1; }
extern struct adc_conf adc_conf[N_ADC];
void send_calib_adc(uint8_t i);
