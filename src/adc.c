//
// adc.c
//

#include "adc.h"
#include "bate.h"
#include "mul.h"
#include "eeprom.h"
#include <avr/interrupt.h>

enum adc_conf_parameter {
	INP = ADC_VIA_ADC_gc,
	INN = ADC_VIA_ADC_gc,
	REF =  10 << ADC_TIMEBASE_gp,
	MODE = ADC_MODE_BURST_SCALING_gc | ADC_START_IMMEDIATE_gc,
	MODE_DIFF = ADC_MODE_BURST_SCALING_gc | ADC_START_IMMEDIATE_gc | ADC_DIFF_bm,
	V_RFP = ADC_MUXPOS_AIN4_gc,
	V_NTC = ADC_MUXPOS_AIN6_gc,
	V_BAT = ADC_MUXPOS_AIN7_gc,
};

struct adc_conf adc_conf[N_ADC] ADC_CONF_ADDR  = {
	{ // Internal Temperature
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_1024MV_gc,
		.inp  = INP | ADC_MUXPOS_TEMPSENSE_gc,
		.calib = 80 * 256,
		.shifts = 1,
	},
	{ // V_CC / 10
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_1024MV_gc,
		.inp  = INP | ADC_MUXPOS_VDDDIV10_gc,
		.calib = 1024 * 10,
		.shifts = 3, // V [×10]
	},
	{ // Batterie Voltage / 11
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_1024MV_gc,
		.inp  = INP | V_BAT,
		.calib = 1024 * 11,
		.shifts = 3, // V [×11]
	},
	{ // NTC biased by V_RF
		.mode = MODE_DIFF,
		.ref  = REF | ADC_REFSEL_1024MV_gc,
		.inp  = INP | V_NTC,
		.inn  = INP | V_RFP,
		.calib = 2048 * 10,
		.shifts = 1, // mV
	},
	{ // NTC biased by V_RF
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_2500MV_gc,
		.inp  = INP | V_NTC,
		.calib = 2500 * 10,
		.shifts = 1,
	},
	{ // NTC biased by V_RF
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_VDD_gc,
		.inp  = INP | V_NTC,
	},
	{ // Transmitter power
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_2500MV_gc,
		.inp  = INP | V_RFP,
		.calib = 2500 * 10,
		.shifts = 1,
	},
	{ // Transmitter power
		.mode = MODE,
		.ref  = REF | ADC_REFSEL_VDD_gc,
		.inp  = INP | V_RFP,
	},
};

uint16_t adc_readings[N_ADC];
uint8_t adc_current = 0xff;

static inline
void start_conversion(const struct adc_conf *c)
{
	ADC.CTRLC = c->ref;
	ADC.MUXNEG = c->inn;
	ADC.MUXPOS = c->inp;
	ADC.COMMAND = c->mode;
}

void init_adc()
{
	PORTA.PIN4CTRL |= PORT_ISC_INPUT_DISABLE_gc;
	PORTA.PIN6CTRL |= PORT_ISC_INPUT_DISABLE_gc;
	PORTA.PIN7CTRL |= PORT_ISC_INPUT_DISABLE_gc;
}

void start_adc()
{
	if (!adc_conf->mode)
		return;
	ADC.CTRLA = ADC_ENABLE_bm | ADC_RUNSTDBY_bm;
	ADC.COMMAND = 0;
	ADC.CTRLB = ADC_PRESC_DIV10_gc;
	ADC.CTRLE = 32 << ADC_SAMPDUR_gp;
	ADC.CTRLF = ADC_SAMPNUM_ACC64_gc | ADC_LEFTADJ_bm;
	adc_current = 0;
	ADC.INTFLAGS = 0xff;
	ADC.INTCTRL = ADC_RESRDY_bm;
	start_conversion(adc_conf);
}

#if 0
ISR(ADC0_RESRDY_vect)
{
	DEBUG_COUNTER(adc_irqs);

	uint8_t i = adc_current;
	if (i >= N_ADC)
		goto stop;
	adc_readings[i] = *(volatile uint16_t *) &ADC.RESULT;
	if (++i < N_ADC) {
		const struct adc_conf *c = adc_conf + i;
		if (c->mode || c->mode == 0xff)
			goto stop;
		adc_current = i;
		start_conversion(c);
		return;
	}
stop:
	ADC.CTRLA = 0;
	adc_current = N_ADC;
}

#else
// This saved (at some earlier point of debugging)
// - 29 of 86 instructions,
// - 24 of 110 clk cycles in the common path
// - 9 of 14 bytes of stack space,
// - and avoids unlikely branches.

ISR(ADC0_RESRDY_vect, ISR_NAKED)
{
	__asm__(
		"push	r24"				"\n\t"
		"in	r24, __SREG__"			"\n\t"
		"push	r24"				"\n\t"
		"push	r25"				"\n\t"
		"push	r30"				"\n\t"
		"push	r31"				"\n\t"
		"lds	r24, adc_current"		"\n\t"
		"cpi	r24, %[NADC]"			"\n\t"
		"brsh 3f"				"\n\t"
		"mov	r30, r24"			"\n\t"
		"ldi	r31, 0"				"\n\t"
		"lsl	r30"				"\n\t"
		"subi	r30, lo8(-(adc_readings))"	"\n\t"
		"sbci	r31, hi8(-(adc_readings))"	"\n\t"
		"lds	r25, %[RESULT]"			"\n\t"
		"st	Z, r25"				"\n\t"
		"lds	r25, %[RESULT]+1"		"\n\t"
		"std	Z+1, r25"			"\n\t"
		"cpi	r24, %[NADC]-1"			"\n\t"
		"brsh 3f"				"\n\t"
		"subi	r24, 0xff"			"\n\t"
		"mov	r30, r24"			"\n\t"
		"ldi	r31, 0"				"\n\t"
		"lsl	r30"				"\n\t"
		"lsl	r30"				"\n\t"
		"lsl	r30"				"\n\t"
		"subi	r30, lo8(-(adc_conf))"		"\n\t"
		"sbci	r31, hi8(-(adc_conf))"		"\n"
	"1:"						"\n\t"
		"ld	r25, Z"				"\n\t"
		"subi	r25, 1"				"\n\t"
		"cpi	r25, 0xfe"			"\n\t"
		"brsh 3f"				"\n\t"
		"ldd	r25, Z+1"			"\n\t"
		"sts	%[CTRLC], r25"			"\n\t"
		"ldd	r25, Z+3"			"\n\t"
		"sts	%[MUXNEG], r25"			"\n\t"
		"ldd	r25, Z+2"			"\n\t"
		"sts	%[MUXPOS], r25"			"\n\t"
		"ld	r25, Z"				"\n\t"
		"sts	%[COMMAND], r25"		"\n"
	"2:"						"\n\t"
		"sts	adc_current, r24"		"\n\t"
		"pop	r31"				"\n\t"
		"pop	r30"				"\n\t"
		"pop	r25"				"\n\t"
		"pop	r24"				"\n\t"
		"out	__SREG__, r24"			"\n\t"
		"pop	r24"				"\n\t"
		"reti"					"\n"
	"3:"						"\n\t"
		"clr	r24"				"\n\t"
		"sts	%[CTRLA], r24"			"\n\t"
		"ldi	r24, %[NADC]"			"\n\t"
		"rjmp 2b"				"\n"
		: :
		  [NADC]	"n" (N_ADC),
		  [RESULT]	"n" (&ADC.RESULT),
		  [CTRLA]	"n" (&ADC.CTRLA),
		  [CTRLC]	"n" (&ADC.CTRLC),
		  [MUXPOS]	"n" (&ADC.MUXPOS),
		  [MUXNEG]	"n" (&ADC.MUXNEG),
		  [COMMAND]	"n" (&ADC.COMMAND)
		);
}
#endif

static inline
int16_t calib_temp(uint16_t a)
{
	uint16_t calib = SIGROW.TEMPSENSE0;
	int16_t offset = (int8_t) SIGROW.TEMPSENSE1;
	a >>= 1;
	a -= offset << 5;
	a = mul16sun(calib, a, 8);
	a -= 8741;
	// [K × 32]
	return a;
}

// saturating addition, signed + signed

static inline
int16_t saddss(int16_t a, int16_t o)
{
	__asm__(
		"	add  	%A[a], %A[o]	\n"
		"	adc  	%B[a], %B[o]	\n"
		"	brvc 1f			\n"
		"	ldi  	%A[a], 0xff	\n"
		"	ldi  	%B[a], 0x7f	\n"
		"	brmi 1f			\n"
		"	adiw 	%[a], 1		\n"
		"1:				\n"
		: [a] "+w" (a)
		: [o] "r"  (o)
		);
	return a;
}

// saturating addition, unsigned + signed

static inline
uint16_t saddus(uint16_t a, int16_t o)
{
	__asm__(
		"	add  	%A[a], %A[o]	\n"
		"	adc  	%B[a], %B[o]	\n"
		"	tst	%B[o]		\n"
		"	brmi 1f			\n"
		"	brcc 3f			\n"
		"	rjmp 2f			\n"
		"1:				\n"
		"	brcs 3f			\n"
		"2:				\n"
		"	ldi  	%A[a], 0xff	\n"
		"	ldi  	%B[a], 0xff	\n"
		"	brcs 3f			\n"
		"	adiw 	%[a], 1		\n"
		"3:				\n"
		: [a] "+w" (a)
		: [o] "r"  (o)
		);
	return a;
}

void send_calib_adc(uint8_t i)
{
	struct adc_conf *conf = adc_conf + i;
	uint8_t sgnd = conf->mode & ADC_DIFF_bm;
	int16_t offset = (int16_t)conf->offset;
	uint16_t calib = conf->calib;
	uint16_t a = adc_readings[i];

	if (conf->inp == ADC_MUXPOS_TEMPSENSE_gc) {
		a = calib_temp(a);
		sgnd = 1;
	}

	if (sgnd)
		a = saddss(a, offset);
	else
		a = saddus(a, offset);

	if (!calib) {
		send_str(" 0x");
		send_hex_word(a);
		return;
	}
	uint16_t c;
	if (sgnd)
		c = calib;
	else {
		c = a;
		a = calib;
	}
	a = mul16sun(a, c, conf->shifts >> 4);

	send_char(' ');
	send_str(decimal_str(a, conf->shifts & 3));
}
