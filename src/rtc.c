//
// rtc.c
//

// !!! int = int8_t

#include "bate.h"
#include "rtc.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define Bit(x) (1<<(x))

volatile uint32_t clock;
volatile uint8_t clock_tick;

void init_rtc(uint8_t p)
{
	if (p>=16)
		p = RTC_PERIOD_CYC1024_gc;
	RTC.CLKSEL = RTC_CLKSEL_INT1K_gc;
	RTC.PITINTCTRL = 1;
	RTC.PITCTRLA = p;
	while (RTC.PITSTATUS & 1) ;
	RTC.PITCTRLA = p | 1;
}

#if 1
ISR(RTC_PIT_vect, ISR_NAKED)
{
	__asm__ ("push r24"            "\n\t"
		 "in r24, __SREG__"    "\n\t"
		 "push r24"            "\n\t"
#ifdef DEBUG
		 "lds r24, debug_data+9"	"\n\t"
		 "subi r24, -1"			"\n\t"
		 "sts debug_data+9, r24"	"\n\t"
#endif
		 "ldi r24,1"           "\n\t"
		 "sts %[flag], r24"    "\n\t"
		 "sts %[tick], r24"    "\n\t"
		 "lds r24, %[clock]"   "\n\t"
		 "subi r24, -1"        "\n\t"
		 "sts %[clock], r24"   "\n\t"
		 "lds r24, %[clock]+1" "\n\t"
		 "sbci r24, -1"        "\n\t"
		 "sts %[clock]+1, r24" "\n\t"
		 "lds r24, %[clock]+2" "\n\t"
		 "sbci r24, -1"        "\n\t"
		 "sts %[clock]+2, r24" "\n\t"
		 "lds r24, %[clock]+3" "\n\t"
		 "sbci r24, -1"        "\n\t"
		 "sts %[clock]+3, r24" "\n\t"
		 "pop r24"             "\n\t"
		 "out __SREG__, r24"   "\n\t"
		 "pop r24"             "\n\t"
		 "reti"                "\n"
		 :[tick] "+m" (clock_tick),
		  [clock] "+m" (clock)
		 :[flag] "n" (&RTC.PITINTFLAGS)
		);
}
#else
ISR(RTC_PIT_vect)
{
	clock_tick = 1;
	RTC.PITINTFLAGS = 1;
	clock++;
}
#endif
