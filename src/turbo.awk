#! /usr/bin/gawk -f

/^T/ {
    if (NF >= 7) {
	phase = 1
	split($0, T)
    }
}
/^p / {
    if (phase==1 && NF==5) {
	phase = 2
	split($0, P)
    } else
	phase = 0
}
/^Q/ {
    if ($2 != "✓")
	phase = 0
    else if (phase==2)
	phase = 3
    else
	emit()
}
/^A/ {
    if (phase==3)
	phase = 4
    else
	phase = 0
}
/^a / {
    V[$2] = $3
    A[$2] = $9
}

function emit() {
    if (phase != 4) {
	phase = 0
	return
    }
    phase = 0
    tT = T[3]
    tU = T[4]
    tF = T[3]+T[5]
    dt = T[6]
    p = P[2]
    Tp = P[4]
    Tc = A["TEMP/1V"]
    Tb = A["NTC-RFP/1V"]
    Tn = A["NTC/2.5V"]
    Vbat = A["BAT/1V"]
    Vdd  = A["VDD/1V"]
    Vrf  = A["RFP/2.5V"]
    printf "%.0f %.1f %.1f %.6f %.2f %.2f %.2f %.2f %.2f %.4f %.4f %.4f\n", \
	tT, tU, tF, dt, p, Tp, Tc, Tn, Tb, Vbat, Vdd, Vrf
}
