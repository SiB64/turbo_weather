#! /usr/bin/make -f

DIR=data
default: $(DIR)/turbo.png $(DIR)/turbo.svg

%.data: %.txt
	./turbo.awk $< > $@

FROM = -1week
DATA = data/turbo.data
PNG = pngcairo size 1200,900 font ",10"
$(DIR)/turbo.png: $(DATA)
	gnuplot -e 'set term $(PNG);set out "$@"' -c turbo.gpt $< $(FROM)

SVG = size 1200,900 name "Turbo"
$(DIR)/turbo.svg: $(DATA)
	gnuplot -e 'set term svg $(SVG);set out "$@"' -c turbo.gpt $< $(FROM)
