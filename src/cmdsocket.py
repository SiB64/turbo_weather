#! /usr/bin/python3

import sys, os, socket

class cmd_socket:

    def __init__(self, path=".cmd.socket", **a):
        self.s = None
        self.c = []
        self.path = None
        if path:
            self.open(path, **a)

    def close(self):
        for c in self.c:
            c.close()
        self.c = []
        self.s.close()
        self.s = None
        if self.path:
            os.unlink(self.path)
 
class cmd_receiver(cmd_socket):

    def open(self, path, blocking=False, force=False):
        if force:
            try:
                os.unlink(path)
            except FileNotFoundError:
                pass
        self.blocking=blocking
        if self.s:
            raise ValueError("socket is not closed")
        self.path = path
        self.s = socket.socket(socket.AF_UNIX)
        self.s.bind(path)
        self.s.listen()
        self.s.setblocking(blocking)
        self.c = []
        self.m = b''

    def poll(self):
        if not self.s:
            return
        try:
            c = self.s.accept()[0]
            self.c.append(c)
            c.setblocking(self.blocking)
            self.s.setblocking(False)
        except BlockingIOError:
            pass
        cc = False
        for i, c in enumerate(self.c):
            try:
                m = c.recv(4096)
                if not m:
                    c.close()
                    self.c[i] = None
                    cc = True
                else:
                    self.exec(m)
            except BlockingIOError:
                pass
        if cc:
            self.c = [c for c in self.c if c]
            if not self.c:
                self.s.setblocking(self.blocking)

    def exec(self, m):
        if self.m:
            m = self.m + m
        mm = m.split(b'\n')
        for m in mm[:-1]:
            self.msg(m)
        self.tail(mm[-1])

    def tail(self, m):
        self.m = m
        if m:
            print(f"socket {self.path}: got tail {m}", file=sys.stderr)
            
    def msg(self, m): 
        print(f"socket {self.path}: got message {m}", file=sys.stderr)

class cmder(cmd_socket):

    def open(self, path):
        if self.s:
            raise ValueError("socket is not closed")
        self.s = socket.socket(socket.AF_UNIX)
        self.s.connect(path)
        self.path = path

    eol = b'\n'
    bol = b''
    verbose = True

    def format(self, m):
        if isinstance(m, str):
            m = m.encode()
        if self.eol and m[-1:] != self.eol:
            m += self.eol
        if self.bol and m[:1] != self.bol:
            m = self.bol + m
        if self.verbose:
            print(f"{self.path}.send({m})", file=sys.stderr)
        return m

    def write(self, m):
        self.pace()
        m = self.format(m)
        if self.s:
            self.s.send(m)

    def pace(self):
        pass

def main():
    import getopt, time
    options, messages = getopt.gnu_getopt(sys.argv[1:], "s:d:rfnN")
    path = ()
    delay = 0.1
    receiver = False
    force = False
    eol = b'\n'
    bol = None
    for o,v in options:
        if o=="-s":
            path = (v,)
        if o=="-d":
            delay = float(v)
        if o=="-r":
            receiver = True
        if o=="-f":
            force = True
        if o=="-n":
            eol = None
        if o=="-N":
            bol = b'\n'

    if receiver:
        s = cmd_receiver(*path, blocking=True, force=force)
        try:
            while True:
                s.poll()
        except KeyboardInterrupt:
            s.close()
            return
    s = cmder(*path)
    s.eol = eol
    s.bol = bol
    for m in messages:
        if delay:
            time.sleep(delay)
        s.write(m)

if __name__=="__main__":
    main()
