//
// uart.h
//

#include <stdint.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

void init_uart(uint16_t div, uint8_t mode);
uint8_t uart_tick();

static inline
uint8_t uart_break_p()
{
	return !(VPORTB.IN & 0x08);
}

void send_char(uint8_t c);
void send_eol();
void send_str(const char *s);
void send_hex_byte(uint8_t b);
void send_hex_word(uint16_t b);
void send_hex(uint8_t header, const uint8_t *s, uint8_t n, uint8_t words);
uint8_t uart_busy();
void command(void);

void send_decimal(uint16_t b,  uint8_t dec);

void parse_command(uint8_t *s, uint8_t n);

static inline
void send_hex_long(uint32_t b)
{
	send_hex_word(b >> 16);
	send_hex_word(b);
}

extern uint8_t uart_cks;
void send_cks();
