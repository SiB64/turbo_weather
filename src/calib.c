//
// calib.c
//

// MS5534-CM.pdf
// https://media.digikey.com/pdf/Data%20Sheets/Measurement%20Specialties%20PDFs/MS5534-CM.pdf

#ifdef CALIB_DEBUG
#    include <stdio.h>
#    define TESTDATA_ADDR
#else
#    include "eeprom.h"
#endif

#include "calib.h"
#include "mul.h"

void bate_calib(const union bate *bate, struct pressure *pt)
{
	uint16_t C1 = bate->W1 >> 1;
	uint16_t C5 = (bate->W1 & 1) << 13 | (bate->W2 & 0xffc0) >> 3;
	uint8_t  C6 = bate->W2 & 0x3f;
	uint16_t C4 = bate->W3 >> 6;
	uint16_t C3 = bate->W4 & 0xffc0;
	uint16_t C2 = (bate->W3 & 0x3f) << 6 | bate->W4 & 0x3f;

	uint16_t D1 = bate->D1;
	uint16_t D2 = bate->D2;

	uint16_t UT1 = C5 + 20224;
	uint16_t  dT = D2 - UT1;

	uint16_t TEMPSENS = C6 + 50;
	int16_t  TEMP = mul16sun(dT, TEMPSENS, 6) + 200;

	int16_t  TCO = C4 - 512;
	uint16_t OFFT1 = C2 << 2;
	// fails when TCO < 0
	uint16_t OFF = OFFT1 + mul16sun(dT, TCO, 4);

	uint16_t SENST1 = C1 + 24576;
	uint16_t TCS = C3;
	uint16_t SENS = SENST1 + mul16sun(dT, TCS, 0);

	int16_t  X = mul16sun(D1 - 7168, SENS, 2) - OFF;
	int16_t  P = mul16sun(X, 80, 8) + 2500;

	pt->T = TEMP + 2732;
	pt->p = P;

#ifdef CALIB_DEBUG

	printf("W1 0x%04x\n", bate->W1);
	printf("W2 0x%04x\n", bate->W2);
	printf("W3 0x%04x\n", bate->W3);
	printf("W4 0x%04x\n", bate->W4);
	printf("D1 0x%04x %6d\n", D1, D1 - 7168);
	printf("D2 0x%04x\n", D2);
	printf("C1 %5u\n",     C1);
	printf("C2 %5u\n",     C2);
	printf("C3 %5d %5d\n", C3>>6, C3);
	printf("C4 %5d\n",     C4);
	printf("C5 %5u %5u\n", C5>>3, C5);
	printf("C6 %5u\n",     C6);
	printf("UT1 %5u\n",  UT1);
	printf("dT        %5d\n",  dT);
	printf("TEMPSENS  %5u\n",  TEMPSENS);
	printf("TEMP      %5d\n",  TEMP);
	printf("TCO       %5d\n",  TCO);
	printf("OFFT1     %5u\n",  OFFT1);
	printf("OFF       %5u\n",  OFF);
	printf("SENST1    %5u\n",  SENST1);
	printf("TCS       %5u %5u\n",  TCS>>6, TCS);
	printf("SENS      %5u\n",  SENS);
	printf("X         %5d\n",  X);
	printf("P         %5d\n",  P);
	printf(" %.1f °C %.1f mbar\n\n", TEMP/10., P/10.);
}

#define MUL_DEBUG
#include "mul.c"

int main()
{
	struct pressure pt;
	for (int i=0; i<5; i++)
		bate_calib(testdata+i, &pt);
	return 0;
#endif
}

const union bate testdata[] TESTDATA_ADDR = {
	{ .w = { 0xA691, 0x0A97, 0x989F, 0xAF28, 0x4896, 0x71F4 }, },
	{ .W = { 0xabaf, 0x3c99, 0xa31a, 0xb589}, .D = { 0x470c, 0x773f }, }, //  21.2 °C, 1021.7 mbar
	{ .W = { 0xabaf, 0x3c99, 0xa31a, 0xb589}, .D = { 0x1a51, 0x6fed }, }, //   7.5 °C, 5.4 mbar
	{ .W = { 0xaa3d, 0x35d9, 0xcbe5, 0xb736}, .D = { 0x4bb7, 0x7487 }, }, //  17.7 °C, 1023.0 mbar
	{ .W = { 0xaa3d, 0x35d9, 0xcbe5, 0xb736}, .D = { 0x1e25, 0x650a }, }, // -11.3 °C, 2.4 mbar
};
