
def mul(a,b):
    return  (a & 0xff) * (b & 0xff)

def mulsu(a,b):
    a &= 0xff
    if a & 0x80:
        a |= 0xff00
    return (a * (b&0xff)) & 0xffff

def mul16su(a, b):
    a &= 0xffff
    b &= 0xffff
    ah = a>>8
    al = a & 0xff
    bh = b>>8
    bl = b & 0xff
    r = mulsu(ah, bh) << 16
    r |= mul(al, bl)
    rr = mulsu(ah, bl)
    if rr & 0x8000:
        r -= 0x1000000
    #c = r & 0x1000000
    r += rr << 8
    #if c != (r & 0x1000000):
    #    r += 0x1000000
    r += mul(al, bh) << 8
    return r & 0xffffffff

def test():
    errors = 0
    for s in range(2):
        for l in range(4):
            for a in range(64, 128):
                for b in range(64, 128):
                    for c in range(9):
                        for d in range(10):
                            e = a << (8-c)
                            if l & 1:
                                e ^= 0xff >> c
                            if (s):
                                e = -e
                            f = b << (9-d)
                            if l & 2:
                                f ^= 0x1ff >> c
                            r = mul16su(e, f)
                            rr = (e*f) & 0xffffffff
                            if r != rr:
                                print("ERROR", e,f,hex(r),hex(rr))
                                errors += 1
                            else:
                                print(e,f,r)
    print(errors, "Errors")
    return errors

def full():
    errors = 0
    for u in range(0x10000):
        print(u)
        for s in range(0x8000):
            r = mul16su(s, u)
            rr = (s*u) & 0xffffffff
            if r != rr:
                print("ERROR", s, u, hex(r), hex(rr))
                errors += 1
        for s in range(0x8000):
            r = mul16su(-s, u)
            rr = (-s*u) & 0xffffffff
            if r != rr:
                print("ERROR", -s, u, hex(r), hex(rr))
                errors += 1
    print(errors, "Errors")
    return errors
