#! /bin/bash

TTY=/dev/ttyUSB0,1200
DATA=/data/blaulicht/temperature
NEWDATA=$(date --utc +$DATA/turbo-%FT%TZ.txt)
OLDDATA=$(ls -1 $DATA/turbo-????-??-??*.txt | tail -1)
LINKDATA=$DATA/turbo.txt

SRC=~/turbo_weather/src

[ -e $NEWDATA ] && exit

if [ "$(stat -c %h $LINKDATA)" -eq "2" ]
then
	rm -vf $LINKDATA
	touch $NEWDATA || exit 0
	ln -v $NEWDATA $LINKDATA
fi

echo $NEWDATA
echo $OLDDATA

$SRC/turbo.py --tty="$TTY" $OLDDATA | tee --append "$NEWDATA"
