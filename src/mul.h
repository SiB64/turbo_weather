//
// mul.h
//

#include <stdint.h>

int16_t mul16su(int16_t s, uint16_t u);
int16_t mul16ss(int16_t a, int16_t b);
int16_t mul16uu(uint16_t a, uint16_t b);
int16_t mul16sun(int16_t s, uint16_t u, uint8_t n);
char *decimal_str(int16_t b, uint8_t dec);

#if defined(MUL_DIVMOD10) || defined(MUL_ALL)
uint16_t divmod10(uint16_t u, uint8_t *mod);
#endif
