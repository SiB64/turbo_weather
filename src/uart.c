//
// uart.c
//

// !!! int = int8_t

#include "bate.h"
#include "mul.h"

#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

#define Bit(x) (1<<(x))

// 10 MHz / 2400 / 16 * 64
#define UART_DIV 16667

__attribute__ ((noinline, noclone))
void init_uart(uint16_t div, uint8_t mode)
{
	if (mode==0xff)
		mode = USART_TXEN_bm;
	mode |= USART_TXEN_bm;
	if (div<64)
		div = UART_DIV;
	USART0.BAUD = div;
	PORTB.DIRSET = Bit(2);
	USART0.CTRLB = mode;
	if (mode & USART_RXEN_bm)
		USART0.CTRLA = USART_RXCIE_bm;
	// `BOTHEDGES` should wake from power down sleep()
	PORTB.PIN3CTRL = PORT_PULLUPEN_bm | PORT_ISC_BOTHEDGES_gc;
}

volatile uint8_t rx_tick;

ISR(PORTB_PORT_vect, ISR_NAKED)
{
	__asm__ ("push r24"         "\n\t"
		 "lds r24, %[stat]" "\n\t"
		 "sts %[stat], r24" "\n\t"
		 "sts rx_tick, r24" "\n\t"
		 "pop r24"          "\n\t"
		 "reti"             "\n"
		 : : [stat] "n" (&VPORTB.INTFLAGS)
		);
}

__attribute__ ((noinline, noclone))
uint8_t uart_tick()
{
	cli();
	uint8_t r = rx_tick;
	rx_tick = 0;
	sei();
	return r;
}

// `uart_tx` buffer size must be a power of 2, max 256.
// Fix `tx()` and `put_char()` / `put_char:`
// when the size changes.
// For now, we can afford half of the available RAM,
// and still have 128 bytes for the stack
#ifdef UART_TX_SMALL
uint8_t uart_tx[128];
#else
uint8_t uart_tx[256];
#endif

#define uart_tx_m (sizeof(uart_tx) - 1)
volatile uint8_t uart_tx_w;
volatile uint8_t uart_tx_r;
volatile uint8_t uart_tx_busy;
uint8_t uart_cks;

#if 0

__attribute__ ((noinline, noclone))
void tx()
{
	// interrupts must be disabled
	if (USART0.STATUS & USART_TXCIF_bm) {
		USART0.STATUS = USART_TXCIF_bm;
		uart_tx_busy = 0;
	}
	// read volatile memory once, for speed
	uint8_t r = uart_tx_r;
	uint8_t w = uart_tx_w;
	while (w - r) {
		uart_tx_busy = w - r;
		if (!(USART0.STATUS & USART_DREIF_bm)) {
			USART0.CTRLA |= USART_DREIE_bm | USART_TXCIE_bm;
			uart_tx_r = r;
			return;
		}
		USART0.TXDATAL = uart_tx[r++ & uart_tx_m];
		// race?  TXC while we were looping?  Drop it.
		USART0.STATUS = USART_TXCIF_bm;
	}
	uart_tx_r = r;
	USART0.CTRLA &=~ USART_DREIE_bm;
}

ISR(USART0_DRE_vect)
{
	DEBUG_COUNTER(tx_irqs);
	tx();
}

#else

__attribute__ ((noinline, noclone))
void tx()
{
	// This uses only six registers, to save stack in the ISR.
	// r0 and r1 need not be saved
	// Avoids unlikely branches.
	// [STATUS] is prepared in advance, to avoid two branches and one memory load,
	// at the cost of one register on the ISR stack (r25)
	__asm__("\n"
		"	lds	r21, uart_tx_r				\n"
		"	lds	r19, uart_tx_w				\n"
		"	lds 	r25, %[CTRLA]				\n"
		"	andi 	r25, ~0x20	; clr DREIE		\n"
		"	lds	r30, %[STATUS]				\n"
		"	ldi	r24, 0x40				\n"
		"	and	r30, r24	; TXCIF			\n"
		"	breq 2f						\n"
		"	sts	%[STATUS], r24	; clr TXCIF		\n"
		"	ldi	r31, 0					\n"
		"	sts	uart_tx_busy, r31			\n"
		"	rjmp 2f						\n"
		"							\n"
		"1:							\n"
		"	mov	r30, r21	;			\n"
		"	subi 	r21, 0xff	; r++ & uart_tx_m	\n"
#ifdef UART_TX_SMALL
		"	andi	r30, 0x7f	;			\n"
#endif
		"	ldi 	r31, 0					\n"
		"	subi 	r30, lo8(-(uart_tx))			\n"
		"	sbci	r31, hi8(-(uart_tx))			\n"
		"	ld 	r24, Z					\n"
		"	sts 	%[TXDATA], r24				\n"
		"	ldi	r24, 0x40				\n"
		"	sts	%[STATUS], r24	; clr TXCIF		\n"
		"2:							\n"
		"	cp	r21, r19				\n"
		"	breq 3f						\n"
		"	sts 	uart_tx_busy, r24	; =0x40		\n"
		"	lds 	r24, %[STATUS]				\n"
		"	sbrc 	r24, 5		; DREIF			\n"
		"	rjmp 1b						\n"
		"	ori	r25, 0x60	; set DREIE TXCIE	\n"
		"3:							\n"		
		"	sts 	%[CTRLA], r25				\n"
		"	sts 	uart_tx_r, r21				\n"
		:
		: [STATUS] "n" (&USART0.STATUS),
		  [CTRLA]  "n" (&USART0.CTRLA),
		  [TXDATA] "n" (&USART0.TXDATAL)
		: "r19", "r21",
		  "r24", "r25",
		  "r30", "r31",
		  "memory"
		);
}

ISR(USART0_DRE_vect, ISR_NAKED)
{
	// Doing this naked is a bit dangerous,
	// but saves five instructions and two bytes stack (r0, r1).
	// OTOH, the C implementation of tx() is not bad either,
	// if we are doing this asm, then we do it agressively.
	__asm__("\n"
		"	push	r24				\n"
		"	in 	r24, __SREG__			\n"
		"	push	r24				\n"
		"	push	r25				\n"
		"	push	r19				\n"
		"	push	r21				\n"
		"	push	r30				\n"
		"	push	r31				\n"
		"						\n"
#ifdef DEBUG
		"	lds 	r24, debug_data + 3		\n"
		"	subi 	r24, 0xff			\n"
		"	sts 	debug_data + 3, r24		\n"
#endif
		"	rcall	tx				\n"
		"						\n"
		"	pop	r31				\n"
		"	pop	r30				\n"
		"	pop	r21				\n"
		"	pop	r19				\n"
		"	pop	r25				\n"
		"	pop	r24				\n"
		"	out	__SREG__, r24			\n"
		"	pop	r24				\n"
		"	reti					\n"
		);
}

#endif

ISR(USART0_TXC_vect, ISR_ALIASOF(USART0_DRE_vect));

uint8_t uart_rx[16];
#define uart_rx_m (sizeof(uart_rx) - 1)

volatile uint8_t uart_rx_w;
volatile uint8_t uart_rx_mes;

#if 0
ISR(USART0_RXC_vect)
{
	DEBUG_COUNTER(rx_irqs);
	// s/while/if/ !
	if (USART0.STATUS & USART_RXCIF_bm) {
		uint8_t c = USART0.RXDATAL;
		DEBUG_POKE(rx_char, c);
		uint8_t w = uart_rx_w;
		uart_rx[w] = c;
		if (w < uart_rx_m)
			uart_rx_w = ++w;
		if (!uart_rx_mes && c=='\n')
			uart_rx_mes = w;
	}
}
#else
ISR(USART0_RXC_vect, ISR_NAKED)
{
	// The loop will in all cases execute exactly once.
	// No need to avoid load and stores inside the loop.
	// The volatileness is also irrelevant, we are the ISR,
	// nobody else is looking.
	// We could just have the ISR be called again
	// in the unusual case there is backlog in the FIFO,
	// and not loop here.
	// We may not even need to test the RXCIF bit.
	// Let's do that …
	__asm__(
		"	push	r24				\n"
		"	in 	r24, __SREG__			\n"
		"	push	r24				\n"
		"	push	r30				\n"
		"	push	r31				\n"
#ifdef DEBUG
		"	lds 	r24, debug_data + 4		\n"
		"	subi 	r24, -1				\n"
		"	sts 	debug_data + 4, r24		\n"
#endif
		"	lds 	r24, %[RXDATA]	 		\n"
		"	lds 	r30, uart_rx_w			\n"
		"	ldi 	r31, 0		 		\n"
		"	subi 	r30, lo8(-(uart_rx)) 		\n"
		"	sbci 	r31, hi8(-(uart_rx)) 		\n"
		"	st 	Z+, r24		 		\n"
#ifdef DEBUG
		"	sts 	debug_data + 5, r24		\n"
#endif
		"	subi	r30, lo8(uart_rx)		\n"
		"	sbrc 	r30, 4 ; log2(sizeof(uart_rx))	\n"
		"	subi	r30, 1				\n"
		"	sts 	uart_rx_w, r30			\n"
		"	cpi 	r24, '\n'	 		\n"
		"	brne 1f			 		\n"
		"	lds	r24, uart_rx_mes		\n"
		"	tst	r24		 		\n"
		"	brne 1f					\n"
		"	sts	uart_rx_mes, r30		\n"
		"1:						\n"
		"	pop	r31				\n"
		"	pop	r30				\n"
		"	pop	r24				\n"
		"	out	__SREG__, r24			\n"
		"	pop	r24				\n"
		"	reti					\n"
		: : [RXDATA] "n" (&USART0.RXDATAL)
		);
}
#endif

#if 0
// These are implemented in `uart_tx.S`

__attribute__ ((noinline, noclone))
uint8_t uart_put_char(uint8_t c)
{
	uint8_t r = uart_tx_r;
	uint8_t w = uart_tx_w;
	uint8_t ww = w + 1;
	if ((ww & uart_tx_m) == (r & uart_tx_m))
		return 1;
	uart_tx[w & uart_tx_m] = c;
	__asm__("" ::: "memory");
	uart_tx_w = ww;
	__asm__("" ::: "memory");
	return 0;
}

__attribute__ ((noinline, noclone))
void send_char(uint8_t c)
{
	uint8_t s = 0;
	uint8_t b;
	while (1) {
		b = uart_put_char(c);
		uart_busy();
		if (!b)
			break;
		sleep_cpu();
		s = 1;
	}
	if (s)
		DEBUG_COUNTER(tx_sleep);
}

__attribute__ ((noinline, noclone))
void send_str(const char *s)
{
	while (*s) {
		uint8_t c = *s++;
		if (!uart_put_char(c))
			continue;
		send_char(c);
	}
	uart_busy();
}

static inline
void rx_dismiss(uint8_t n)
{
	cli();
	uint8_t i = 0;
	uint8_t m = 0;
	uint8_t w = uart_rx_w;
	if (w != uart_rx_m)
		while (n < w) {
			uint8_t c = uart_rx[n++];
			uart_rx[i++] = c;
			if (!m && c=='\n')
				m = i;
		}
	uart_rx_mes = m;
	uart_rx_w = i;
	sei();	
}

__attribute__ ((noinline, noclone))
uint8_t uart_busy()
{
	cli();
	tx();
	sei();
	return uart_tx_busy;
}

static __attribute__ ((noinline, noclone))
void send_hex_nibble(uint8_t b)
{
	b += '0';
	if (b>'9')
		b += '@' - '9'; 
	send_char(b);
}

__attribute__ ((noinline, noclone))
void send_hex_byte(uint8_t b)
{
	send_hex_nibble(b >> 4);
	send_hex_nibble(b & 0xf);
}

__attribute__ ((noinline, noclone))
void send_hex_word(uint16_t b)
{
	send_hex_byte(b >> 8);
	send_hex_byte(b);
}

__attribute__ ((noinline, noclone))
void send_hex(uint8_t header, const uint8_t *s, uint8_t n, uint8_t words)
{
	// dump little endian words.
	send_char(header);
	while (n--) {
		if (words & 1)
			send_char(' ');
		uint8_t l = *(s++);
		if (words & 2 && n & 1) {
			send_hex_byte(*(s++));
			n--;
		}
		send_hex_byte(l);
	}
	send_char('\n');
}

void command(void)
{
	uint8_t m = uart_rx_mes;
	if (!m)
		return;
	uint8_t *s = uart_rx;
	uint8_t i = 0;
	uint8_t c;
	send_str("R ");
	while (i++ < m) {
		c = *s++;
		send_char(c);
	}
	if (c != '\n') 
		send_char('\n');
	else
		parse_command(uart_rx, m);
	rx_dismiss(m);
}

#endif
