#!/usr/bin/python3
# encoding: UTF-8

import sys, time

debug = 0

def calibrate(Word, D):

    C=[0]*7
    C[1] = Word[1] >> 1
    C[2] = ((Word[3] & 0x3f)<<6) | (Word[4] & 0x3f) 
    C[3] = Word[4]>>6
    C[4] = Word[3]>>6
    C[5] = ((Word[1] & 1)<<10) | (Word[2]>>6)
    C[6] = Word[2] & 0x3f

    if debug:
       sys.stdout.write("""
    C1 = %d
    C2 = %d
    C3 = %d
    C4 = %d
    C5 = %d
    C6 = %d
    """ % tuple(C[1:7]))
    
    UT1 = 8*C[5]+20224
    dT = D[2] - UT1
    TEMP = 200 + dT*(C[6]+50)/1024
    
    if debug:
       print(f"{D[3]=} {UT1=} {dT=} {TEMP=:.2f} °C", file=sys.stderr)
    
    OFF = C[2]*4 + ((C[4]-512)*dT)/4096
    SENS = C[1] + (C[3]*dT)/1024 + 24576
    X = (SENS * (D[1]-7168))/16384 - OFF
    P = X*10/32 + 2500
    
    if debug:
       print(f"{D[1]=} {OFF=:.2f} {SENS=:.2f} {P=:.2f}", file=sys.stderr)

    return (TEMP,P)


def main():
    data = [0]*7
    import fileinput
    for l in fileinput.input(errors='backslashreplace'):
        if l[0]=='T':
            t = int(l.split()[1],0)
            print(f"{time.time():.1f} {t} {time.strftime('%FT%TZ', time.gmtime(t))}")
        if l[0]=='W':
            data[1:5] = [int(ll,16) for ll in l.split()[1:]]
        elif l[0]=='D':
            data[5:] = [int(ll,16) for ll in l.split()[1:]]
            if data[1]:
                T,p = calibrate(data, data[4:])
                print(f"{T/10:.2f} °C, {p/10:.2f} mbar")
        elif l[0] in "PV":
            print(l.strip())

if __name__=="__main__":
    main()
