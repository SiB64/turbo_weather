# PcbBOM Version 1.0
# Date: So 21 Apr 2024 10:57:09 GMT UTC
# Author: 
# Title: TURBO - PCB BOM
# Quantity, Description, Value, RefDes
# --------------------------------------------
C0603           -BYP_100nF         1/1   C4
C0603           100nF              6/6   C1 C10 C11 C12 C2 C3
C0603           SMD-LED            1/1   D1
C0603.fp        -SD:_10kΩ          1/1   R13
C0603.fp        10MΩ               1/1   R9
C0603.fp        10kΩ               4/4   R11 R5 R7 R8
C0603.fp        10kΩ_NTC           1/1   R6
C0603.fp        1MΩ                1/1   R10
C0603.fp        2.2MΩ              1/1   R4
C0603.fp        220kΩ              1/1   R2
C0603.fp        3.3MΩ              1/1   R3
C0603.fp        330kΩ              1/1   R1
C0603.fp        ∞Ω                 1/1   R12
KEYSTONE-1025-7 6V                 1/1   B1
MS5534C         MS5534C            1/1   U2
P1206           10µF              11/11  C20 C21 C22 C23 C24 C25 C26 C27 C28 C29
                                 C30
SIL_100_3       HE_100_1×3         1/1   J1
SIL_100_4       WRL-10534          1/1   U3
SOIC_150_14     ATtiny4x4SS        1/1   U1
SOT23_5         LT1761-SD          1/1   U4
SOT23_5         LT1761-SD_/_-BYP   1/1   U5
SUBD9_PINS      D9_pigtail         1/1   CONN1
gseboard        turbo              1/1   BOARD
